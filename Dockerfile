FROM arm64v8/openjdk:17-slim-buster
ARG JAR_FILE
COPY target/${JAR_FILE} /station-monitoring-service.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/station-monitoring-service.jar"]