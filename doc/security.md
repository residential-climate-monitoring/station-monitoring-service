# OAuth

In order to be able to authenticate a user using OAuth2 the following environment variables must be set:

* `OAUTH_CLIENT_SECRET`

  The client secret is used to retrieve access tokens from the identity provider.

* `KEYSTORE_PASSWORD` 

  The keystore password is used to access the certificate in the keystore to enable https.

# Disabling security

To disable security, explicitly specify `SPRING_PROFILES_ACTIVE=unsecured`
