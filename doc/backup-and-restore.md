# Backing up

The following scripts creates a tar file of each volume. Before running, make sure the directory `home/pi/backup` exists.

```shell
docker run --rm --volumes-from station-monitoring-service_monitoring-platform_1 -v ~/backup:/backup ubuntu bash -c "cd /prometheus && tar cvf /backup/prometheus.tar ."
docker run --rm --volumes-from station-monitoring-service_monitoring-dashboard_1 -v ~/backup:/backup ubuntu bash -c "cd /var/lib/grafana && tar cvf /backup/grafana.tar ."
docker run --rm --volumes-from station-monitoring-service_db_1 -v ~/backup:/backup ubuntu bash -c "cd /var/lib/postgresql/data && tar cvf /backup/postgres.tar ."
```

The following script exports the tar files to a remote system.

```shell
scp -r ~/backup pi@raspberrypi-400.local:/home/pi/backup
```

# Restoring

Source: https://medium.com/@loomchild/backup-restore-docker-named-volumes-350397b8e362

To restore the volumes:

```shell
docker run --rm -v backup_monitoring-platform_1:/prometheus -v ~/backup:/backup alpine sh -c "rm -rf /prometheus/* /prometheus/..?* /prometheus/.[!.]* ; tar -C /prometheus/ -xvf /backup/prometheus.tar"
docker run --rm -v backup_monitoring-dashboard_1:/var/lib/grafana -v ~/backup:/backup alpine sh -c "rm -rf /var/lib/grafana/* /var/lib/grafana/..?* /var/lib/grafana/.[!.]* ; tar -C /var/lib/grafana/ -xvf /backup/grafana.tar"
docker run --rm -v backup_data-db:/var/lib/postgresql/data -v ~/backup:/backup alpine sh -c "rm -rf /var/lib/postgresql/data/* /var/lib/postgresql/data/..?* /var/lib/postgresql/data/.[!.]* ; tar -C /var/lib/postgresql/data/ -xvf /backup/postgres.tar"
```

Note: The restored volumes are prefixed with `backup_` instead of `station-monitoring-service_` in this example.
