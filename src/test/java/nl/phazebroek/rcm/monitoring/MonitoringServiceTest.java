/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.monitoring;

import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;
import static nl.phazebroek.rcm.measurement.MeasurementStubFactory.createMeasurement;
import static org.assertj.core.api.BDDAssertions.then;
import static org.awaitility.Awaitility.await;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.util.concurrent.AtomicDouble;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.search.Search;
import java.util.List;
import java.util.Map;
import nl.phazebroek.rcm.measurement.Measurement;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class MonitoringServiceTest {

    @InjectMocks
    private MonitoringService monitoringService;

    @Mock
    private MeterRegistry meterRegistry;

    @Captor
    private ArgumentCaptor<List<Tag>> tagsCaptor;

    private Measurement measurement;

    private AtomicDouble gauge;

    @BeforeEach
    void setUp() {
        measurement = createMeasurement(TEMPERATURE, TEMPERATURE_VAL);
    }

    @Test
    void gaugeIsUpdatedWithValueFromMeasurement() {
        gauge = new AtomicDouble(0);
        when(meterRegistry.gauge(any(), anyList(), any(Number.class))).thenReturn(gauge);

        monitoringService.updateGauge(measurement);

        then(gauge.get()).isEqualTo(measurement.getValue());
    }

    @Test
    void gaugeIsCreatedWithExpectedName() {
        gauge = new AtomicDouble(0);
        when(meterRegistry.gauge(any(), anyList(), any(Number.class))).thenReturn(gauge);

        monitoringService.updateGauge(measurement);

        verify(meterRegistry).gauge(eq("sensor.readings"), anyList(), any(Number.class));
    }

    @Test
    void gaugeIsCreatedWithCorrectTag() {
        gauge = new AtomicDouble(0);
        when(meterRegistry.gauge(any(), anyList(), any(Number.class))).thenReturn(gauge);

        monitoringService.updateGauge(measurement);

        verify(meterRegistry).gauge(any(), tagsCaptor.capture(), any(Number.class));
        assertTags();
    }

    @Test
    void gaugeIsCached() {
        gauge = new AtomicDouble(0);
        when(meterRegistry.gauge(any(), anyList(), any(Number.class))).thenReturn(gauge);

        monitoringService.updateGauge(measurement);
        var secondMeasurement = createMeasurement(TEMPERATURE, TEMPERATURE_VAL + 0.1);
        monitoringService.updateGauge(secondMeasurement);

        then(gauge.get()).isEqualTo(secondMeasurement.getValue());
        verify(meterRegistry, atMostOnce()).gauge(any(), anyList(), any(Number.class));
    }

    @Test
    void counterIsCreatedWithExpectedName() {
        var counter = mock(Counter.class);
        when(meterRegistry.counter(any(), anyList())).thenReturn(counter);

        monitoringService.incrementCounter(measurement);

        verify(meterRegistry).counter(eq("rcm.measurement.counter"), anyList());
    }

    @Test
    void counterIsIncremented() {
        var counter = mock(Counter.class);
        when(meterRegistry.counter(any(), anyList())).thenReturn(counter);

        monitoringService.incrementCounter(measurement);

        verify(counter).increment();
    }

    @Test
    void counterIsCreatedWithCorrectTags() {
        var counter = mock(Counter.class);
        when(meterRegistry.counter(any(), anyList())).thenReturn(counter);

        monitoringService.incrementCounter(measurement);

        verify(meterRegistry).counter(anyString(), tagsCaptor.capture());
        assertTags();
    }

    @Test
    void counterIsCached() {
        var counter = mock(Counter.class);
        when(meterRegistry.counter(any(), anyList())).thenReturn(counter);

        monitoringService.incrementCounter(measurement);
        monitoringService.incrementCounter(measurement);

        verify(meterRegistry, atMostOnce()).counter(anyString(), anyList());
    }

    @Test
    void removeIdleGauges() {
        ReflectionTestUtils.setField(monitoringService, "maxIdleTime", 1000);
        gauge = new AtomicDouble(0);
        when(meterRegistry.gauge(any(), anyList(), any(Number.class))).thenReturn(gauge);
        var search = mock(Search.class);
        when(meterRegistry.find(any())).thenReturn(search);
        when(search.tag(any(), anyString())).thenReturn(search);
        var meter = mock(Gauge.class);
        when(search.gauges()).thenReturn(List.of(meter));
        monitoringService.updateGauge(measurement);
        var updatedAt = System.currentTimeMillis();

        Map<String, AtomicDouble> gauges = (Map<String, AtomicDouble>) ReflectionTestUtils.getField(monitoringService, "gauges");
        Map<String, Long> gaugesUpdatedAt = (Map<String, Long>) ReflectionTestUtils.getField(monitoringService, "gaugesUpdatedAt");

        var key = STATION_NAME + "." + TEMPERATURE.getName();
        then(gauges.get(key).get()).isEqualTo(measurement.getValue());
        then(gaugesUpdatedAt.get(key)).isCloseTo(updatedAt, Offset.offset(250L));
        var gaugeCaptor = ArgumentCaptor.forClass(Gauge.class);
        await().untilAsserted(() -> {
            monitoringService.removeIdleGauges();
            verify(meterRegistry).find("sensor.readings");
            verify(search).tag("station", STATION_NAME);
            verify(search).tag("metric", TEMPERATURE.getName());
            verify(search).gauges();
            verify(meterRegistry).remove(gaugeCaptor.capture());
            var removedGauge = gaugeCaptor.getValue();
            then(removedGauge).isEqualTo(meter);
            then(gauges).isEmpty();
            then(gaugesUpdatedAt).isEmpty();
        });
    }

    @Test
    void removeIdleGaugesLeavesActiveGaugesIntact() throws InterruptedException {
        ReflectionTestUtils.setField(monitoringService, "maxIdleTime", 1000);
        gauge = new AtomicDouble(0);
        when(meterRegistry.gauge(any(), anyList(), any(Number.class))).thenReturn(gauge);
        monitoringService.updateGauge(measurement);

        Map<String, AtomicDouble> gauges = (Map<String, AtomicDouble>) ReflectionTestUtils.getField(monitoringService, "gauges");
        Map<String, Long> gaugesUpdatedAt = (Map<String, Long>) ReflectionTestUtils.getField(monitoringService, "gaugesUpdatedAt");

        Thread.sleep(500);

        monitoringService.removeIdleGauges();
        verify(meterRegistry, never()).remove(any(Meter.class));
        then(gauges).isNotEmpty();
        then(gaugesUpdatedAt).isNotEmpty();
    }

    private void assertTags() {
        var tags = tagsCaptor.getValue();
        then(tags).containsExactly(
            Tag.of("station", measurement.getStation().getName()),
            Tag.of("indoor", "true"),
            Tag.of("metric", TEMPERATURE.getName())
        );
    }

}
