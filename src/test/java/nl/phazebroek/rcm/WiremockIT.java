/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import nl.phazebroek.rcm.WiremockIT.WiremockConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.cloud.contract.wiremock.WireMockConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * Base class for integration tests with Wiremock.
 */
@Import(WiremockConfiguration.class)
@AutoConfigureWireMock(port = 0)
public abstract class WiremockIT extends BaseIT {

    /**
     * Disabled HTTPS for wiremock by setting the httpsPort to -1 explicitly.
     */
    @TestConfiguration
    static class WiremockConfiguration {
        @Bean
        WireMockConfigurationCustomizer optionsCustomizer() {
            return options -> options.httpsPort(-1);
        }
    }

    protected ResponseDefinitionBuilder aResponseWithBodyFile(String fileName) {
        return aResponse()
            .withBodyFile(fileName)
            .withStatus(200)
            .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    }

    protected ResponseDefinitionBuilder a5xxResponse() {
        return aResponse().withStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
    }

}
