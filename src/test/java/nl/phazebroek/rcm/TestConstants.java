/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.metric.MetricFactory;

public abstract class TestConstants {

    public static final UUID STATION_ID = UUID.randomUUID();
    public static final String STATION_NAME = "test-station";

    public static final UUID SENSOR_ID = UUID.randomUUID();
    public static final String SENSOR_SHT31D = "SHT31-D";
    public static final String SENSOR_TSL2591 = "TSL2591";

    public static final UUID METRIC_ID = UUID.randomUUID();

    @Deprecated
    public static final Metric TEMPERATURE = MetricFactory.createMetric("temperature", "°C");
    @Deprecated
    public static final Metric HUMIDITY = MetricFactory.createMetric("humidity", "%");
    @Deprecated
    public static final Metric LUX = MetricFactory.createMetric("lux", "lx");
    @Deprecated
    public static final Metric VISIBLE_LIGHT = MetricFactory.createMetric("visible-light");
    @Deprecated
    public static final Metric IR_LIGHT = MetricFactory.createMetric("IR-light");
    @Deprecated
    public static final Set<Metric> METRICS_SHT31D = Set.of(TEMPERATURE, HUMIDITY);
    @Deprecated
    public static final Set<Metric> METRICS_TSL2591 = Set.of(LUX, VISIBLE_LIGHT, IR_LIGHT);

    public static final String METRIC_TEMPERATURE = "temperature";
    public static final String METRIC_HUMIDITY = "humidity";
    public static final String METRIC_LUX = "lux";
    public static final String METRIC_LUX_UNIT = "lx";
    public static final String METRIC_VISIBLE_LIGHT = "visible-light";
    public static final String METRIC_IR_LIGHT = "IR-light";

    public static final String UNIT_CELSIUS = "*C";
    public static final String UNIT_FAHRENHEIT = "*F";
    public static final String UNIT_PERCENT = "%";

    public static final double TEMPERATURE_VAL = 21.42;
    public static final double HUMIDITY_VAL = 50.42;
    public static final double BATTERY_PERCENTAGE = 0.99;
    public static final int LIGHT_VAL = 1234;
    public static final String MOTION_SENSOR_MODEL_ID = "SML001";
    public static final String OUTDOOR_SENSOR_MODEL_ID = "SML002";


    public static final List<String> METRIC_SHT31D_NAMES = List.of(METRIC_HUMIDITY, METRIC_TEMPERATURE);
    public static final List<String> METRICS_TSL2591_NAMES = List.of(METRIC_LUX, METRIC_VISIBLE_LIGHT, METRIC_IR_LIGHT);

    public static final String GAIN_SETTING = "tsl2591-gain";
    public static final String GAIN_LOW = "GAIN_LOW";
    public static final String GAIN_MEDIUM = "GAIN_MEDIUM";

    public static final String ADMIN = "ADMIN";
    public static final String SUBMIT_REPORT = "SCOPE_submit:report";

    public static final String HUE_COMMON_UNIQUE_ID_1 = "01:23:45:67:89:01:a1:b2";
    public static final String HUE_COMMON_UNIQUE_ID_2 = "12:34:56:78:90:12:b2:c3";
    // The name of the sensor as defined in RCM.
    public static final String HUE_CONFIG_MOTION_SENSOR_NAME = "Hue Motion Sensor";
    // The name of the sensors as returned by the Hue Bridge API.
    public static final String HUE_MOTION_SENSOR_NAME_1 = "Motion sensor 1";
    public static final String HUE_MOTION_SENSOR_NAME_2 = "Motion sensor 2";

}
