/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.station.StationFactory.createStation;

import nl.phazebroek.rcm.metric.Metric;

public abstract class MeasurementStubFactory {

    public static Measurement createMeasurement(Metric metric, double value) {
        var station = createStation();
        return new Measurement(station, metric, value);
    }

}
