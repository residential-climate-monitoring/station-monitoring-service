/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.TestConstants.HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.HUMIDITY_VAL;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;
import static nl.phazebroek.rcm.measurement.MeasurementReportDtoFactory.createMeasurementReport;
import static nl.phazebroek.rcm.measurement.MeasurementStubFactory.createMeasurement;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.monitoring.MonitoringService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MeasurementServiceTest {

    @InjectMocks
    private MeasurementService measurementService;

    @Mock
    private MeasurementReportValidator measurementReportValidator;

    @Mock
    private MeasurementMapper measurementMapper;

    @Mock
    private MonitoringService monitoringService;

    @Test
    void reportIsProcessed() {
        var report = createMeasurementReport();
        var measurement1 = createMeasurement(TEMPERATURE, TEMPERATURE_VAL);
        var measurement2 = createMeasurement(HUMIDITY, HUMIDITY_VAL);
        when(measurementMapper.toMeasurements(any())).thenReturn(List.of(measurement1, measurement2));

        measurementService.processMeasurementReport(report);

        var order = Mockito.inOrder(measurementReportValidator, measurementMapper, monitoringService);
        order.verify(measurementReportValidator).validate(report);
        order.verify(measurementMapper).toMeasurements(report);
        order.verify(monitoringService).updateGauge(measurement1);
        order.verify(monitoringService).updateGauge(measurement2);
        order.verify(monitoringService).incrementCounter(measurement1);
        order.verify(monitoringService).incrementCounter(measurement2);
    }

    @Test
    void invalidReportIsNotProcessed() {
        var report = createMeasurementReport();
        doThrow(new BadRequestException("")).when(measurementReportValidator).validate(any());

        assertThrows(BadRequestException.class, () -> measurementService.processMeasurementReport(report));
        verifyNoInteractions(monitoringService);
    }
}
