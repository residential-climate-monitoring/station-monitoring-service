/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.TestConstants.HUMIDITY_VAL;
import static nl.phazebroek.rcm.TestConstants.METRIC_HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.METRIC_TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;
import static nl.phazebroek.rcm.measurement.MeasurementReportDtoFactory.createMeasurementReport;
import static nl.phazebroek.rcm.metric.MetricFactory.createHumidityMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createTemperatureMetric;
import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.metric.MetricRepository;
import nl.phazebroek.rcm.station.Station;
import nl.phazebroek.rcm.station.StationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MeasurementMapperTest {

    @InjectMocks
    private MeasurementMapperImpl measurementMapper;

    @Mock
    private StationService stationService;

    @Mock
    private MetricRepository metricRepository;

    @Test
    void measurementReportIsMappedToListOfMeasurements() {
        var station = mock(Station.class);
        when(stationService.findStationByName(any())).thenReturn(Optional.of(station));
        var measurementReport = createMeasurementReport();
        Metric temperatureMetric = createTemperatureMetric();
        when(metricRepository.findByName(METRIC_TEMPERATURE)).thenReturn(Optional.of(temperatureMetric));
        Metric humidityMetric = createHumidityMetric();
        when(metricRepository.findByName(METRIC_HUMIDITY)).thenReturn(Optional.of(humidityMetric));

        var measurements = measurementMapper.toMeasurements(measurementReport);

        then(measurements).hasSize(2);
        then(measurements).extracting(Measurement::getStation).contains(station);
        then(measurements).extracting(Measurement::getMetric)
            .containsExactlyInAnyOrder(humidityMetric, temperatureMetric);
        then(measurements).extracting(Measurement::getValue)
            .containsExactlyInAnyOrder(HUMIDITY_VAL, TEMPERATURE_VAL);
        verify(stationService).findStationByName(STATION_NAME);
    }

    @Test
    void exceptionIsThrownWhenNoStationByNameWasFound() {
        when(stationService.findStationByName(any())).thenReturn(Optional.empty());
        var measurementReport = new MeasurementReportDto();

        assertThrows(NoSuchElementException.class,
                () -> measurementMapper.toMeasurements(measurementReport));
    }

    @Test
    void mapNullMeasurementReportReturnsDefaultValue() {
        then(measurementMapper.toMeasurements(null)).isEqualTo(new ArrayList<>());
    }
}
