/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.TestConstants.SUBMIT_REPORT;
import static nl.phazebroek.rcm.measurement.MeasurementReportDtoFactory.createMeasurementReport;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.Map;
import nl.phazebroek.rcm.BaseIT;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

class MeasurementReportControllerIT extends BaseIT {

    @Test
    @WithMockUser(authorities = SUBMIT_REPORT)
    void submitMeasurementReport() throws Exception {
        setupDefaultStation();

        var measurementReport = createMeasurementReport();

        postMeasurementReport(measurementReport)
            .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(authorities = SUBMIT_REPORT)
    void cannotSubmitMeasurementReportForUnknownStation() throws Exception {
        var measurementReport = createMeasurementReport();

        postMeasurementReport(measurementReport)
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = SUBMIT_REPORT)
    void cannotSubmitMeasurementReportWithoutStation() throws Exception {
        var measurementReport = createMeasurementReport().stationName(null);

        postMeasurementReport(measurementReport)
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = SUBMIT_REPORT)
    void cannotSubmitMeasurementReportWithoutMeasurements() throws Exception {
        var measurementReport = createMeasurementReport().measurements(null);

        postMeasurementReport(measurementReport)
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = SUBMIT_REPORT)
    void cannotSubmitMeasurementReportWitEmptyMeasurements() throws Exception {
        var measurementReport = createMeasurementReport().measurements(Collections.emptyMap());

        postMeasurementReport(measurementReport)
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(authorities = SUBMIT_REPORT)
    void cannotSubmitMeasurementReportWitInvalidMetric() throws Exception {
        var measurementReport = createMeasurementReport().measurements(Map.of("swag", 9000.0));

        postMeasurementReport(measurementReport)
            .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void submitMeasurementReportForbidden() throws Exception {
        setupDefaultStation();

        postMeasurementReport(createMeasurementReport())
            .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    void submitMeasurementReportUnauthorized() throws Exception {
        postMeasurementReport(createMeasurementReport())
            .andExpect(status().isUnauthorized());
    }

    private ResultActions postMeasurementReport(MeasurementReportDto measurementReport) throws Exception {
        return mockMvc.perform(post("/measurement-reports")
            .content(objectMapper.writeValueAsString(measurementReport))
            .contentType(MediaType.APPLICATION_JSON)
            .with(csrf())
        );
    }
}
