/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.TestConstants.METRICS_SHT31D;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.measurement.MeasurementReportDtoFactory.createMeasurementReport;
import static nl.phazebroek.rcm.station.StationFactory.createStation;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Optional;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.metric.DefaultMetric;
import nl.phazebroek.rcm.station.StationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MeasurementReportValidatorTest {

    @InjectMocks
    private MeasurementReportValidator measurementReportValidator;

    @Mock
    private StationService stationService;

    @Test
    void validReportIsAccepted() {
        var station = createStation();
        when(stationService.findStationByName(any())).thenReturn(Optional.of(station));
        when(stationService.getSupportedMetrics(any())).thenReturn(METRICS_SHT31D);

        var report = createMeasurementReport();

        measurementReportValidator.validate(report);

        verify(stationService).findStationByName(STATION_NAME);
        verify(stationService).getSupportedMetrics(station);
    }

    @Test
    void reportWitUnknownStationNameIsRejected() {
        when(stationService.findStationByName(any())).thenReturn(Optional.empty());

        var report = createMeasurementReport();
        report.setStationName("nonexisting-station");

        var exception = assertThrows(BadRequestException.class,
            () -> measurementReportValidator.validate(report)
        );
        assertThat(exception.getMessage(), is("Unknown station 'nonexisting-station'"));
    }

    @Test
    void reportWithUnknownMetricIsRejected() {
        var station = createStation();
        when(stationService.findStationByName(any())).thenReturn(Optional.of(station));
        when(stationService.getSupportedMetrics(any())).thenReturn(METRICS_SHT31D);

        var report = createMeasurementReport();
        report.setMeasurements(Map.of(
            "alcohol", 0.0,
            "swag", 9000.0));

        var exception = assertThrows(BadRequestException.class,
            () -> measurementReportValidator.validate(report)
        );
        assertThat(exception.getMessage(), is("Station '" + station.getName() + "' does not support the following "
            + "metric(s): 'alcohol, swag'"));
    }

    @Test
    void reportWithUnsupportedMetricByStationIsRejected() {
        var station = createStation();
        when(stationService.findStationByName(any())).thenReturn(Optional.of(station));
        when(stationService.getSupportedMetrics(any())).thenReturn(METRICS_SHT31D);

        var report = createMeasurementReport();
        report.setMeasurements(Map.of(DefaultMetric.VOC_GAS.getName(), 0.0));

        var exception = assertThrows(BadRequestException.class,
            () -> measurementReportValidator.validate(report)
        );
        assertThat(exception.getMessage(), is("Station '" + station.getName() + "' does not support the following "
            + "metric(s): '" + DefaultMetric.VOC_GAS.getName() + "'"));
    }
}
