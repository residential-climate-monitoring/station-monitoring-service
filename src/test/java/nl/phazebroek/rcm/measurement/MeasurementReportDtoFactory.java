/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.TestConstants.HUMIDITY_VAL;
import static nl.phazebroek.rcm.TestConstants.METRIC_HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.METRIC_TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;

import java.util.Map;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;

public class MeasurementReportDtoFactory {

    public static MeasurementReportDto createMeasurementReport() {
        var measurementReport = new MeasurementReportDto();
        measurementReport.setStationName(STATION_NAME);
        measurementReport.setMeasurements(Map.of(
                METRIC_TEMPERATURE, TEMPERATURE_VAL,
                METRIC_HUMIDITY, HUMIDITY_VAL));
        return measurementReport;
    }

}
