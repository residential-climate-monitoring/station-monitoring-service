/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.ADMIN;
import static nl.phazebroek.rcm.TestConstants.METRICS_TSL2591_NAMES;
import static nl.phazebroek.rcm.TestConstants.METRIC_SHT31D_NAMES;
import static nl.phazebroek.rcm.TestConstants.SENSOR_ID;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.SENSOR_TSL2591;
import static nl.phazebroek.rcm.metric.MetricFactory.createHumidityMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createTemperatureMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createTsl2591Metrics;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;
import static nl.phazebroek.rcm.sensor.SensorFactory.createTsl2591Sensor;
import static nl.phazebroek.rcm.sensor.SensorStubFactory.newCreateSensorDto;
import static org.assertj.core.api.BDDAssertions.then;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import nl.phazebroek.rcm.BaseIT;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.api.model.GetSensorsResponse;
import nl.phazebroek.rcm.api.model.SensorDto;
import nl.phazebroek.rcm.api.model.UpdateSensorDto;
import nl.phazebroek.rcm.metric.Metric;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

class SensorControllerIT extends BaseIT {

    @Nested
    class GetSensors {
        @Test
        @WithMockUser
        void getSensors() throws Exception {
            var sensor = setupSensor(createSht31dSensor());

            var response = mockMvc.perform(get("/sensors"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            var metrics = sensor.getMetrics().stream()
                .map(Metric::getName)
                .toList();

            var getSensorsResponse = objectMapper.readValue(response, GetSensorsResponse.class);
            then(getSensorsResponse.getSensors()).extracting(SensorDto::getId).containsExactly(sensor.getId());
            then(getSensorsResponse.getSensors()).extracting(SensorDto::getName).containsExactly(sensor.getName());
            then(getSensorsResponse.getSensors()).flatExtracting(SensorDto::getMetrics).containsExactlyInAnyOrderElementsOf(metrics);
        }

        @Test
        @WithAnonymousUser
        void getSensorsUnauthorized() throws Exception {
            mockMvc.perform(get("/sensors"))
                .andExpect(status().isUnauthorized());
        }
    }

    @Nested
    class AddSensor {

        @Test
        @WithMockUser(roles = ADMIN)
        void addSensor() throws Exception {
            metricRepository.saveAll(List.of(createTemperatureMetric(), createHumidityMetric()));
            var createSensorDto = newCreateSensorDto();

            postSensor(createSensorDto)
                .andExpect(status().isNoContent());

            var sensor = sensorRepository.findByName(SENSOR_SHT31D).orElseThrow();
            then(sensor.getMetrics()).extracting(Metric::getName).containsAll(METRIC_SHT31D_NAMES);
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addSensorWithoutName() throws Exception {
            var createSensorDto = newCreateSensorDto().name(null);

            postSensor(createSensorDto)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addSensorWithInvalidMetric() throws Exception {
            var createSensorDto = newCreateSensorDto().metrics(List.of("swag"));

            postSensor(createSensorDto)
                .andExpect(status().isBadRequest());
        }

        @ParameterizedTest
        @NullAndEmptySource
        @WithMockUser(roles = ADMIN)
        void addSensorWithoutMetrics(List<String> metrics) throws Exception {
            var createSensorDto = newCreateSensorDto().metrics(metrics);

            postSensor(createSensorDto)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addSensorWithEmptyMetrics() throws Exception {
            var createSensorDto = newCreateSensorDto().metrics(Collections.emptyList());

            postSensor(createSensorDto)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addSensorWithNonUniqueName() throws Exception {
            setupSensor(createSht31dSensor());

            var createSensorDto = newCreateSensorDto();

            postSensor(createSensorDto)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithAnonymousUser
        void addSensorsUnauthorized() throws Exception {
            postSensor(newCreateSensorDto())
                .andExpect(status().isUnauthorized());

        }

        @Test
        @WithMockUser
        void addSensorsForbidden() throws Exception {
            postSensor(newCreateSensorDto())
                .andExpect(status().isForbidden());

        }

        private ResultActions postSensor(CreateSensorDto body) throws Exception {
            return mockMvc.perform(post("/sensors")
                .content(objectMapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }

    @Nested
    class UpdateSensor {

        @Test
        @WithMockUser(roles = ADMIN)
        void updateSensorName() throws Exception {
            var sensor = setupSensor(createSht31dSensor());
            var request = new UpdateSensorDto().name(SENSOR_TSL2591);

            putSensor(request, sensor.getId())
                .andExpect(status().isNoContent());

            sensorRepository.findByName(SENSOR_TSL2591).orElseThrow();
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateSensorMetrics() throws Exception {
            var sensor = setupSensor(createSht31dSensor());
            metricRepository.saveAll(createTsl2591Metrics());
            var request = new UpdateSensorDto().metrics(METRICS_TSL2591_NAMES);

            putSensor(request, sensor.getId())
                .andExpect(status().isNoContent());

            sensor = sensorRepository.findByName(SENSOR_SHT31D).orElseThrow();
            then(sensor.getMetrics()).extracting(Metric::getName).containsAll(METRICS_TSL2591_NAMES);
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateSensorNoFields() throws Exception {
            var sensor = setupSensor(createSht31dSensor());
            var request = new UpdateSensorDto();

            putSensor(request, sensor.getId())
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateSensorNonUniqueName() throws Exception {
            var sht31dSensor = setupSensor(createSht31dSensor());
            setupSensor(createTsl2591Sensor());
            var request = new UpdateSensorDto().name(SENSOR_TSL2591);

            putSensor(request, sht31dSensor.getId())
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateSensorNotFound() throws Exception {
            var request = new UpdateSensorDto().name(SENSOR_TSL2591);

            putSensor(request, SENSOR_ID)
                .andExpect(status().isNotFound());
        }

        @Test
        void updateSensorUnauthorized() throws Exception {
            putSensor(new UpdateSensorDto(), SENSOR_ID)
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void updateSensorForbidden() throws Exception {
            putSensor(new UpdateSensorDto(), SENSOR_ID)
                .andExpect(status().isForbidden());
        }

        private ResultActions putSensor(UpdateSensorDto body, UUID id) throws Exception {
            return mockMvc.perform(put("/sensors/{id}", id)
                .content(objectMapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }
}
