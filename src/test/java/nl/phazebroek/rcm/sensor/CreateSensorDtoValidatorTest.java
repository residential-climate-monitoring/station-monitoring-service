/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.sensor.SensorStubFactory.newCreateSensorDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.metric.MetricValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CreateSensorDtoValidatorTest {

    @InjectMocks
    private CreateSensorDtoValidator validator;

    @Mock
    private SensorRepository sensorRepository;

    @Mock
    private MetricValidator metricValidator;

    @Test
    void validSensorIsAccepted() {
        when(sensorRepository.existsByName(any())).thenReturn(false);
        var createSensorDto = newCreateSensorDto();

        validator.validate(createSensorDto);

        verify(metricValidator).validate(createSensorDto.getMetrics());
        verify(sensorRepository).existsByName(SENSOR_SHT31D);
    }

    @Test
    void sensorWithInvalidMetricIsRejected() {
        var e = new BadRequestException("test");
        doThrow(e).when(metricValidator).validate(anyList());
        var createSensorDto = newCreateSensorDto();

        var exception = assertThrows(BadRequestException.class, () -> validator.validate(createSensorDto));
        assertThat(exception.getMessage(), is(e.getMessage()));
    }

    @Test
    void sensorWithNonUniqueNameIsRejected() {
        when(sensorRepository.existsByName(any())).thenReturn(true);
        var createSensorDto = newCreateSensorDto();

        var exception = assertThrows(BadRequestException.class, () -> validator.validate(createSensorDto));
        assertThat(exception.getMessage(), is("Name of the sensor must be unique"));
    }
}
