/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.SENSOR_ID;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;
import static nl.phazebroek.rcm.sensor.SensorStubFactory.newUpdateSensorDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.metric.MetricValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UpdateSensorDtoValidatorTest {

    @InjectMocks
    private UpdateSensorDtoValidator validator;

    @Mock
    private SensorRepository sensorRepository;

    @Mock
    private MetricValidator metricValidator;

    @Test
    void validSensorIsAccepted() {
        when(sensorRepository.findByName(any())).thenReturn(Optional.empty());
        var updateSensorDto = newUpdateSensorDto();

        validator.validate(updateSensorDto, SENSOR_ID);

        verify(metricValidator).validate(updateSensorDto.getMetrics());
        verify(sensorRepository).findByName(SENSOR_SHT31D);
    }

    @Test
    void validSensorWithSameNameIsAccepted() {
        var sensor = createSht31dSensor();
        sensor.setId(SENSOR_ID);
        when(sensorRepository.findByName(any())).thenReturn(Optional.of(sensor));
        var updateSensorDto = newUpdateSensorDto();

        validator.validate(updateSensorDto, SENSOR_ID);
    }

    @Test
    void sensorWithInvalidMetricIsRejected() {
        var e = new BadRequestException("test");
        doThrow(e).when(metricValidator).validate(anyList());
        var updateSensorDto = newUpdateSensorDto();

        var exception = assertThrows(BadRequestException.class, () -> validator.validate(updateSensorDto, SENSOR_ID));
        assertThat(exception.getMessage(), is(e.getMessage()));
    }

    @Test
    void sensorWithNonUniqueNameIsRejected() {
        var sensor = createSht31dSensor();
        sensor.setId(UUID.randomUUID());
        when(sensorRepository.findByName(any())).thenReturn(Optional.of(sensor));
        var updateSensorDto = newUpdateSensorDto();

        var exception = assertThrows(BadRequestException.class, () -> validator.validate(updateSensorDto, SENSOR_ID));
        assertThat(exception.getMessage(), is("Name of the sensor must be unique"));
    }

}