/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.METRIC_SHT31D_NAMES;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;

import java.util.List;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.api.model.SensorDto;
import nl.phazebroek.rcm.api.model.UpdateSensorDto;

public abstract class SensorStubFactory {

    public static SensorDto newSensorDto() {
        return new SensorDto()
            .name(SENSOR_SHT31D)
            .metrics(METRIC_SHT31D_NAMES);
    }

    public static CreateSensorDto newCreateSensorDto() {
        return new CreateSensorDto()
            .name(SENSOR_SHT31D)
            .metrics(METRIC_SHT31D_NAMES);
    }

    public static CreateSensorDto newCreateSensorDtoWithMetrics(List<String> metrics) {
        return newCreateSensorDto().metrics(metrics);
    }

    public static UpdateSensorDto newUpdateSensorDto() {
        return new UpdateSensorDto()
            .name(SENSOR_SHT31D)
            .metrics(METRIC_SHT31D_NAMES);
    }
}
