/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.METRICS_TSL2591;
import static nl.phazebroek.rcm.TestConstants.METRICS_TSL2591_NAMES;
import static nl.phazebroek.rcm.TestConstants.SENSOR_ID;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.SENSOR_TSL2591;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;
import static nl.phazebroek.rcm.sensor.SensorStubFactory.newCreateSensorDto;
import static nl.phazebroek.rcm.sensor.SensorStubFactory.newSensorDto;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.NotFoundException;
import nl.phazebroek.rcm.api.model.UpdateSensorDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SensorServiceTest {

    @InjectMocks
    private SensorService sensorService;

    @Mock
    private SensorRepository sensorRepository;

    @Mock
    private SensorMapper sensorMapper;

    @Mock
    private CreateSensorDtoValidator createSensorDtoValidator;

    @Mock
    private UpdateSensorDtoValidator updateSensorDtoValidator;

    @Test
    void getSensor() {
        var sensor = createSht31dSensor();
        when(sensorRepository.findById(any())).thenReturn(Optional.of(sensor));

        var result = sensorService.getSensor(SENSOR_ID);

        then(result).isEqualTo(sensor);
        verify(sensorRepository).findById(SENSOR_ID);
    }

    @Test
    void getSensorNotFound() {
        var e = catchThrowable(() -> sensorService.getSensor(SENSOR_ID));
        then(e).isInstanceOf(NotFoundException.class);
        then(e).hasMessage("Sensor with id " + SENSOR_ID + " not found");
    }

    @Test
    void findSensorsByName() {
        var sensor = createSht31dSensor();
        when(sensorRepository.findAllByNameIn(any())).thenReturn(List.of(sensor));

        var sensors = sensorService.findSensorsByName(List.of(SENSOR_SHT31D));

        assertThat(sensors, contains(sensor));
        verify(sensorRepository).findAllByNameIn(List.of(SENSOR_SHT31D));
    }

    @Test
    void getSensors() {
        var sensor = createSht31dSensor();
        when(sensorRepository.findAll()).thenReturn(List.of(sensor));
        var sensorDto = newSensorDto();
        when(sensorMapper.toSensorDtos(any())).thenReturn(List.of(sensorDto));

        var sensors = sensorService.getSensors();

        assertThat(sensors, contains(sensorDto));
        verify(sensorRepository).findAll();
        verify(sensorMapper).toSensorDtos(argThat(arg -> arg.contains(sensor)));
    }

    @Test
    void sensorExistsWithName() {
        when(sensorRepository.existsByName(any())).thenReturn(true);

        var result = sensorService.sensorExistsWithName(SENSOR_SHT31D);

        assertThat(result, is(true));
        verify(sensorRepository).existsByName(SENSOR_SHT31D);
    }

    @Test
    void sensorDoesNotExistWithName() {
        when(sensorRepository.existsByName(any())).thenReturn(false);

        var result = sensorService.sensorExistsWithName(SENSOR_SHT31D);

        assertThat(result, is(false));
    }

    @Test
    void addSensor() {
        var createSensorDto = newCreateSensorDto();
        var sensor = createSht31dSensor();
        when(sensorMapper.toSensor(createSensorDto)).thenReturn(sensor);
        when(sensorRepository.save(any())).then(returnsFirstArg());

        sensorService.addSensor(createSensorDto);

        var inOrder = Mockito.inOrder(createSensorDtoValidator, sensorMapper, sensorRepository);
        inOrder.verify(createSensorDtoValidator).validate(createSensorDto);
        inOrder.verify(sensorMapper).toSensor(createSensorDto);
        inOrder.verify(sensorRepository).save(sensor);
    }

    @Test
    void updateSensorName() {
        var sensor = createSht31dSensor();
        when(sensorRepository.findById(any())).thenReturn(Optional.of(sensor));
        var request = new UpdateSensorDto().name(SENSOR_TSL2591);

        sensorService.updateSensor(SENSOR_ID, request);

        then(sensor.getName()).isEqualTo(SENSOR_TSL2591);

        var inOrder = Mockito.inOrder(sensorRepository, updateSensorDtoValidator);
        inOrder.verify(sensorRepository).findById(SENSOR_ID);
        inOrder.verify(updateSensorDtoValidator).validate(request, SENSOR_ID);
        inOrder.verify(sensorRepository).save(sensor);
    }

    @Test
    void updateSensorMetrics() {
        var sensor = createSht31dSensor();
        when(sensorRepository.findById(any())).thenReturn(Optional.of(sensor));
        when(sensorMapper.toMetrics(any())).thenReturn(METRICS_TSL2591);
        var request = new UpdateSensorDto().metrics(METRICS_TSL2591_NAMES);

        sensorService.updateSensor(SENSOR_ID, request);

        then(sensor.getMetrics()).containsExactlyElementsOf(METRICS_TSL2591);

        verify(sensorMapper).toMetrics(METRICS_TSL2591_NAMES);
    }

    @Test
    void updateSensorValidationFailed() {
        var sensor = createSht31dSensor();
        when(sensorRepository.findById(any())).thenReturn(Optional.of(sensor));
        var request = new UpdateSensorDto().name(SENSOR_TSL2591);
        doThrow(new BadRequestException("test")).when(updateSensorDtoValidator).validate(any(), any());

        var e = catchThrowable(() -> sensorService.updateSensor(SENSOR_ID, request));
        then(e).isInstanceOf(BadRequestException.class);
        then(e).hasMessage("test");
    }


}
