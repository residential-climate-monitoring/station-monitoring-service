/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.SENSOR_TSL2591;
import static nl.phazebroek.rcm.metric.MetricFactory.createSht31dMetrics;
import static nl.phazebroek.rcm.metric.MetricFactory.createTsl2591Metrics;

public class SensorFactory {

    public static Sensor createSht31dSensor() {
        var sensor = new Sensor();
        sensor.setName(SENSOR_SHT31D);
        sensor.setMetrics(createSht31dMetrics());
        return sensor;
    }

    public static Sensor createTsl2591Sensor() {
        var sensor = new Sensor();
        sensor.setName(SENSOR_TSL2591);
        sensor.setMetrics(createTsl2591Metrics());
        return sensor;
    }

}
