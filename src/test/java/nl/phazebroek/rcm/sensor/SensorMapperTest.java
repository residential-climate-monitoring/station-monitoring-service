/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.TestConstants.METRIC_SHT31D_NAMES;
import static nl.phazebroek.rcm.TestConstants.SENSOR_ID;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.metric.MetricFactory.createTemperatureMetric;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;
import static nl.phazebroek.rcm.sensor.SensorStubFactory.newCreateSensorDtoWithMetrics;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import nl.phazebroek.rcm.api.model.SensorDto;
import nl.phazebroek.rcm.metric.MetricRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SensorMapperTest {

    @InjectMocks
    private SensorMapperImpl sensorMapper;

    @Mock
    private MetricRepository metricRepository;

    @Test
    void fullSensorIsMappedToSensorDto() {
        var sensor = createSht31dSensor();
        sensor.setId(SENSOR_ID);

        var sensorDto = sensorMapper.toSensorDto(sensor);

        assertThat(sensorDto.getId(), is(SENSOR_ID));
        assertThat(sensorDto.getName(), is(SENSOR_SHT31D));
        assertThat(sensorDto.getMetrics(), is(METRIC_SHT31D_NAMES));
    }

    @Test
    void mapSensorDtoWithDuplicateMetricNamesResultsInSingleMetric() {
        var metricNames = List.of("temperature", "temperature");
        var temperatureMetric = createTemperatureMetric();
        var metrics = Set.of(temperatureMetric);
        given(metricRepository.findAllByNameIn(metricNames)).willReturn(metrics);
        var createSensorDto = newCreateSensorDtoWithMetrics(metricNames);

        var sensor = sensorMapper.toSensor(createSensorDto);

        assertThat(sensor.getMetrics(), is(metrics));
    }

    @Test
    void listOfSensorsIsMappedToSensorDtos() {
        var sensors = List.of(createSht31dSensor(), createSht31dSensor());

        var sensorDtos = sensorMapper.toSensorDtos(sensors);

        assertThat(sensorDtos, hasSize(2));
    }

    @Test
    void mapToSensorWithNullReturnsDefaultValue() {
        assertThat(sensorMapper.toSensor(null), is(instanceOf(Sensor.class)));
    }

    @Test
    void mapToSensorDtoWithNullReturnsDefaultValue() {
        assertThat(sensorMapper.toSensorDto(null), is(instanceOf(SensorDto.class)));
    }

    @Test
    void mapToSensorDtosWithNullReturnsDefaultValue() {
        assertThat(sensorMapper.toSensorDtos(null), is(instanceOf(ArrayList.class)));
    }

    @Test
    void mapToMetricsWithNullReturnsDefaultValue() {
        assertThat(sensorMapper.toMetrics(null), is(instanceOf(HashSet.class)));
    }

    @Test
    void mapToMetricNamesWithNullReturnsDefaultValue() {
        assertThat(sensorMapper.toMetricNames(null), is(instanceOf(ArrayList.class)));
    }
}
