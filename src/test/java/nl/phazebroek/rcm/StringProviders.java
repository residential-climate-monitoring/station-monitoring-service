/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm;

import java.util.stream.Stream;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * A utility class providing various random string values for parameterized tests.
 */
public abstract class StringProviders {

    /**
     * Provide a stream of random valid sensor names.
     */
    static Stream<String> randomSensorNames() {
        return Stream.of(
            RandomStringUtils.randomAlphanumeric(1),
            RandomStringUtils.randomAlphanumeric(1, 50),
            RandomStringUtils.randomAlphanumeric(50)
        );
    }

}
