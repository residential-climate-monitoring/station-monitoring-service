/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static java.util.Collections.emptyMap;
import static nl.phazebroek.rcm.TestConstants.ADMIN;
import static nl.phazebroek.rcm.TestConstants.GAIN_LOW;
import static nl.phazebroek.rcm.TestConstants.GAIN_SETTING;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.SENSOR_TSL2591;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;
import static nl.phazebroek.rcm.sensor.SensorFactory.createTsl2591Sensor;
import static nl.phazebroek.rcm.settings.SensorSettingsStubFactory.createSettings;
import static nl.phazebroek.rcm.station.StationFactory.createStation;
import static nl.phazebroek.rcm.station.StationFactory.createStationWithSensors;
import static nl.phazebroek.rcm.station.StationStubFactory.createCreateStationRequest;
import static nl.phazebroek.rcm.station.StationStubFactory.createSensorWithSettingsDto;
import static nl.phazebroek.rcm.station.StationStubFactory.createUpdateStationRequestWithSensors;
import static org.assertj.core.api.BDDAssertions.fail;
import static org.assertj.core.api.BDDAssertions.then;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Map;
import nl.phazebroek.rcm.BaseIT;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.GetStationsResponse;
import nl.phazebroek.rcm.api.model.SensorSettingsDto;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.api.model.StationDto;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.settings.Settings;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

class StationControllerIT extends BaseIT {

    @Nested
    class GetStations {

        @Test
        @WithMockUser
        void getStations() throws Exception {
            var station = setupDefaultStation();

            var responseContent = mockMvc
                .perform(get("/stations"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            var getStationsResponse = objectMapper.readValue(responseContent, GetStationsResponse.class);
            var result = getStationsResponse.getStations().get(0);
            then(result.getId()).isEqualTo(station.getId());
            then(result.getName()).isEqualTo(station.getName());
            var sensors = result.getSensors();
            then(sensors).hasSize(1);
            var sensor = sensors.get(0);
            then(sensor.getName()).isEqualTo(SENSOR_SHT31D);
        }

        @Test
        @WithMockUser
        void getStationsIncludingSensorSettings() throws Exception {
            var tsl2591Sensor = createTsl2591Sensor();
            setupSensor(tsl2591Sensor);

            var station = setupDefaultStation();
            station.getSensors().add(tsl2591Sensor);
            stationRepository.save(station);

            var sensorSettings = new Settings();
            sensorSettings.setStation(station);
            sensorSettings.setSensor(tsl2591Sensor);
            Map<String, String> settings = Map.of(GAIN_SETTING, GAIN_LOW);
            sensorSettings.setSettings(settings);
            sensorSettingsRepository.save(sensorSettings);

            var responseContent = mockMvc
                .perform(get("/stations"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            var getStationsResponse = objectMapper.readValue(responseContent, GetStationsResponse.class);
            var result = getStationsResponse.getStations().get(0);
            then(result.getSensors()).hasSize(2);
            then(result.getSensors().get(1).getSettings()).containsExactlyEntriesOf(settings);
        }

        @Test
        @WithMockUser
        void getStationsEmpty() throws Exception {
            mockMvc
                .perform(get("/stations"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("stations").isEmpty());
        }

        @Test
        @WithAnonymousUser
        void getStationsUnauthorized() throws Exception {
            mockMvc
                .perform(get("/stations"))
                .andExpect(status().isUnauthorized());
        }
    }

    @Nested
    class AddStation {

        @ParameterizedTest
        @MethodSource("nl.phazebroek.rcm.StringProviders#randomSensorNames")
        @WithMockUser(roles = ADMIN)
        void addStationRandomName(String name) throws Exception {
            setupSensor(createSht31dSensor());

            var request = createCreateStationRequest().name(name);

            postStation(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty());
        }

        @ParameterizedTest
        @ValueSource(booleans = {true, false})
        @WithMockUser(roles = ADMIN)
        void addStationRandomIndoor(boolean indoor) throws Exception {
            setupSensor(createSht31dSensor());

            var request = createCreateStationRequest().indoor(indoor);

            postStation(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addStationMultipleSensorsAndSettings() throws Exception {
            setupSensor(createSht31dSensor());
            setupSensor(createTsl2591Sensor());

            var request = createCreateStationRequest();
            request.getSensors()
                .add(createSensorWithSettingsDto(SENSOR_TSL2591, Map.of(GAIN_SETTING, GAIN_LOW)));

            postStation(request)
                .andExpect(status().isOk());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addStationNonUniqueName() throws Exception {
            var station = createStation();
            station.getSensors().forEach(StationControllerIT.this::setupSensor);
            stationRepository.save(station);

            var request = createCreateStationRequest().name(station.getName());

            postStation(request)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void addStationNonExistingSensor() throws Exception {
            var request = createCreateStationRequest();
            request.getSensors().add(new SensorWithSettingsDto().name("non-existing-sensor"));

            postStation(request)
                .andExpect(status().isBadRequest());
        }

        @ParameterizedTest
        @NullAndEmptySource
        @WithMockUser(roles = ADMIN)
        void addStationWithBadName(String name) throws Exception {
            var request = createCreateStationRequest().name(name);

            postStation(request)
                .andExpect(status().isBadRequest());
        }

        @ParameterizedTest
        @NullAndEmptySource
        @WithMockUser(roles = ADMIN)
        void addStationWithoutSensors(List<SensorWithSettingsDto> sensors) throws Exception {
            var request = createCreateStationRequest().sensors(sensors);

            postStation(request)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithAnonymousUser
        void addStationUnauthorized() throws Exception {
            var request = createCreateStationRequest();

            postStation(request)
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void addStationForbidden() throws Exception {
            var request = createCreateStationRequest();

            postStation(request)
                .andExpect(status().isForbidden());
        }

        private ResultActions postStation(CreateStationRequest request) throws Exception {
            return mockMvc.perform(post("/stations")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }

    @Nested
    class GetStation {
        @Test
        @WithMockUser
        void getStation() throws Exception {
            var station = createStation();
            station.getSensors().forEach(StationControllerIT.this::setupSensor);
            stationRepository.save(station);

            var sensor = station.getSensors().get(0);
            var sensorSettings = new Settings();
            sensorSettings.setStation(station);
            sensorSettings.setSensor(sensor);
            Map<String, String> settings = Map.of("some-setting", "someValue");
            sensorSettings.setSettings(settings);
            sensorSettingsRepository.save(sensorSettings);

            var responseContent = mockMvc
                .perform(get("/stations/{name}", STATION_NAME))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            var stationDto = objectMapper.readValue(responseContent, StationDto.class);
            then(stationDto.getId()).isEqualTo(station.getId());
            then(stationDto.getName()).isEqualTo(station.getName());
            then(stationDto.getIndoor()).isEqualTo(station.isIndoor());
            var sensorWithSettingsDto = stationDto.getSensors().get(0);
            then(sensorWithSettingsDto.getName()).isEqualTo(sensor.getName());
            then(sensorWithSettingsDto.getSettings()).containsExactlyEntriesOf(settings);
        }
    }

    @Nested
    class UpdateStation {

        @Test
        @WithMockUser(roles = ADMIN)
        void updateStation() throws Exception {
            var sht31d = setupSensor(createSht31dSensor());
            var tsl2591 = setupSensor(createTsl2591Sensor());
            var station = createStationWithSensors(sht31d);
            stationRepository.save(station);

            var sensor = createSensorWithSettingsDto(SENSOR_TSL2591, Map.of(GAIN_SETTING, GAIN_LOW));
            var request = createUpdateStationRequestWithSensors(sensor);

            putStation(station.getName(), request)
                .andExpect(status().isNoContent());

            var updatedStation = stationRepository.findStationByName(station.getName()).orElseGet(() -> fail("expected station"));
            then(updatedStation.getSensors()).extracting(Sensor::getName).containsExactly(SENSOR_TSL2591);
            then(updatedStation.isIndoor()).isTrue();

            var actualSensorSettings = sensorSettingsRepository.findByStationAndSensor(station, tsl2591)
                .orElseGet(() -> fail("Expected sensor settings"));
            then(actualSensorSettings.getSettings()).containsExactlyEntriesOf(Map.of(GAIN_SETTING, GAIN_LOW));
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateStationNonExistingSensor() throws Exception {
            setupDefaultStation();

            var sensor = createSensorWithSettingsDto("non-existing-sensor", emptyMap());
            var request = createUpdateStationRequestWithSensors(sensor);

            putStation(STATION_NAME, request)
                .andExpect(status().isBadRequest());
        }

        @ParameterizedTest
        @NullAndEmptySource
        @WithMockUser(roles = ADMIN)
        void updateStationEmptySensors(List<SensorWithSettingsDto> sensors) throws Exception {
            setupDefaultStation();

            var request = new UpdateStationRequest().sensors(sensors);

            putStation(STATION_NAME, request)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateStationNothing() throws Exception {
            setupDefaultStation();

            var request = new UpdateStationRequest();

            putStation(STATION_NAME, request)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithAnonymousUser
        void updateStationUnauthorized() throws Exception {
            var request = createUpdateStationRequestWithSensors();

            putStation(STATION_NAME, request)
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void updateStationForbidden() throws Exception {
            var request = createUpdateStationRequestWithSensors();

            putStation(STATION_NAME, request)
                .andExpect(status().isForbidden());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void updateStationNonExistingStation() throws Exception {
            var request = createUpdateStationRequestWithSensors();

            putStation(STATION_NAME, request)
                .andExpect(status().isNotFound());
        }

        private ResultActions putStation(String name, UpdateStationRequest request) throws Exception {
            return mockMvc.perform(put("/stations/{name}", name)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }

    @Nested
    class GetSettings {

        @Test
        @WithMockUser
        void getSensorSettingsEmpty() throws Exception {
            setupDefaultStation();

            getSensorSettings()
                .andExpect(status().isOk())
                .andExpect(jsonPath("settings").isEmpty());
        }

        @Test
        @WithMockUser
        void getSensorSettingsNotEmpty() throws Exception {
            var station = createStation();
            station.getSensors().forEach(StationControllerIT.this::setupSensor);
            var savedStation = stationRepository.save(station);
            var sensors = station.getSensors();
            sensors.forEach(sensor -> {
                var sensorSettings = createSettings(savedStation, sensor, Map.of(GAIN_SETTING, GAIN_LOW));
                sensorSettingsRepository.save(sensorSettings);
            });

            getSensorSettings()
                .andExpect(status().isOk())
                .andExpect(jsonPath("settings." + GAIN_SETTING).value(GAIN_LOW));
        }

        @Test
        @WithAnonymousUser
        void getSensorSettingsNotAuthorized() throws Exception {
            getSensorSettings()
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void getSensorSettingsNoExistingSensorSettings() throws Exception {
            var station = createStation();
            station.getSensors().forEach(StationControllerIT.this::setupSensor);
            stationRepository.save(station);

            getSensorSettings()
                .andExpect(status().isOk())
                .andExpect(jsonPath("settings").isEmpty());
        }

        private ResultActions getSensorSettings() throws Exception {
            return mockMvc.perform(get("/stations/{name}/sensors/{sensor}/settings", STATION_NAME, SENSOR_SHT31D));
        }
    }

    @Nested
    class UpdateSettings {

        @Test
        @WithMockUser(roles = ADMIN)
        void setSensorSettings() throws Exception {
            var station = setupDefaultStation();
            var sensor = station.getSensors().get(0);

            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW))
                .andExpect(status().isNoContent());

            var sensorSettings = sensorSettingsRepository.findByStationAndSensor(station, sensor)
                .orElseGet(() -> fail("Expected sensor settings"));
            then(sensorSettings.getSettings()).containsAllEntriesOf(Map.of(GAIN_SETTING, GAIN_LOW));
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void setSensorSettingsIsIdempotent() throws Exception {
            var station = setupDefaultStation();
            var sensor = station.getSensors().get(0);

            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW)).andExpect(status().isNoContent());
            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW)).andExpect(status().isNoContent());

            then(sensorSettingsRepository.count()).isEqualTo(1);
            var sensorSettings = sensorSettingsRepository.findByStationAndSensor(station, sensor)
                .orElseGet(() -> fail("Expected sensor settings"));
            then(sensorSettings.getSettings()).hasSize(1);
        }

        @ParameterizedTest
        @NullAndEmptySource
        @WithMockUser(roles = ADMIN)
        void setSensorSettingsNullAndEmpty(Map<String, Object> settings) throws Exception {
            setupDefaultStation();

            putSensorSettings(settings)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void setSensorSettingsNonExistingStation() throws Exception {
            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW))
                .andExpect(status().isNotFound());
        }

        @Test
        @WithMockUser(roles = ADMIN)
        void setSensorSettingsNonExistingSensor() throws Exception {
            setupDefaultStation();

            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW), "non-existing-sensor")
                .andExpect(status().isNotFound());
        }

        @Test
        @WithAnonymousUser
        void setSensorSettingsUnauthorized() throws Exception {
            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW))
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void setSensorSettingsForbidden() throws Exception {
            putSensorSettings(Map.of(GAIN_SETTING, GAIN_LOW))
                .andExpect(status().isForbidden());
        }

        private ResultActions putSensorSettings(Map<String, Object> settings) throws Exception {
            return putSensorSettings(settings, SENSOR_SHT31D);
        }

        private ResultActions putSensorSettings(Map<String, Object> settings, String sensor) throws Exception {
            var request = new SensorSettingsDto().settings(settings);
            return mockMvc.perform(put("/stations/{name}/sensors/{sensor}/settings", STATION_NAME, sensor)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }
}
