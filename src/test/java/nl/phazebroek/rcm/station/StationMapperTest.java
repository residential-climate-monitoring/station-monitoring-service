/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static nl.phazebroek.rcm.settings.SensorSettingsStubFactory.createSensorSettingsDto;
import static nl.phazebroek.rcm.station.StationFactory.createStation;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import nl.phazebroek.rcm.settings.SettingsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit test the {@link StationMapper}. Note: the {@link StationMapperImpl_} is the generated class that directly extends from the mapper.
 */
@ExtendWith(MockitoExtension.class)
class StationMapperTest {

    @InjectMocks
    private StationMapperImpl_ stationMapper;

    @Mock
    private SettingsService sensorSettingsService;

    @Test
    void toSensorWithSettingsDto() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        var sensorSettingsDto = createSensorSettingsDto();
        when(sensorSettingsService.getSensorSettingsDto(any(), any())).thenReturn(sensorSettingsDto);

        var result = stationMapper.toSensorWithSettingsDtos(station);

        then(result).hasSize(1);
        var sensorSettingsWithDto = result.get(0);
        then(sensorSettingsWithDto.getName()).isEqualTo(sensor.getName());
        then(sensorSettingsWithDto.getSettings()).isEqualTo(sensorSettingsDto.getSettings());
        verify(sensorSettingsService).getSensorSettingsDto(station, sensor);
    }
}
