/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static java.util.Arrays.asList;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.STATION_ID;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.api.model.StationDto;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;

public abstract class StationStubFactory {

    public static CreateStationRequest createCreateStationRequest() {
        var sensors = new ArrayList<SensorWithSettingsDto>();
        sensors.add(createSensorWithSettingsDto());
        return new CreateStationRequest()
            .name(STATION_NAME)
            .indoor(true)
            .sensors(sensors);
    }

    public static SensorWithSettingsDto createSensorWithSettingsDto() {
        return createSensorWithSettingsDto(SENSOR_SHT31D, null);
    }

    public static SensorWithSettingsDto createSensorWithSettingsDto(String name, Map<String, Object> settings) {
        return new SensorWithSettingsDto()
            .name(name)
            .settings(settings);
    }

    public static StationDto createStationDto() {
        var sensors = new ArrayList<SensorWithSettingsDto>();
        sensors.add(createSensorWithSettingsDto());
        return new StationDto()
            .name(STATION_NAME)
            .sensors(sensors);
    }

    public static StationDto createStationDtoWithId() {
        return createStationDto()
            .id(STATION_ID);
    }

    public static UpdateStationRequest createUpdateStationRequestWithSensors(SensorWithSettingsDto... sensors) {
        if (sensors.length > 0) {
            return createUpdateStationRequestWithSensors(asList(sensors));
        } else {
            var list = new ArrayList<SensorWithSettingsDto>();
            list.add(createSensorWithSettingsDto());
            return createUpdateStationRequestWithSensors(list);
        }
    }

    public static UpdateStationRequest createUpdateStationRequestWithSensors(List<SensorWithSettingsDto> sensors) {
        var updateStationRequest = new UpdateStationRequest();
        updateStationRequest.setSensors(sensors);
        return updateStationRequest;
    }

}
