/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.SENSOR_TSL2591;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.station.StationStubFactory.createCreateStationRequest;
import static nl.phazebroek.rcm.station.StationStubFactory.createSensorWithSettingsDto;
import static nl.phazebroek.rcm.station.StationStubFactory.createUpdateStationRequestWithSensors;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;
import nl.phazebroek.rcm.sensor.SensorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StationValidatorTest {

    @InjectMocks
    private StationValidator stationValidator;

    @Mock
    private SensorService sensorService;

    @Mock
    private StationRepository stationRepository;

    @Test
    void stationWithNonUniqueNameIsRejected() {
        when(stationRepository.existsByName(any())).thenReturn(true);

        var request = createCreateStationRequest();

        var e = catchThrowable(() -> stationValidator.validate(request));

        then(e)
            .isInstanceOf(BadRequestException.class)
            .hasMessage("Name of the station must be unique");
    }

    @Test
    void stationWithKnownSensorIsAccepted() {
        when(stationRepository.existsByName(any())).thenReturn(false);
        when(sensorService.sensorExistsWithName(any())).thenReturn(true);
        var request = createCreateStationRequest();

        stationValidator.validate(request);
        verify(sensorService).sensorExistsWithName(SENSOR_SHT31D);
        verify(stationRepository).existsByName(STATION_NAME);
    }

    @Test
    void stationWithNonExistingSensorIsRejected() {
        when(stationRepository.existsByName(any())).thenReturn(false);
        when(sensorService.sensorExistsWithName(any())).thenReturn(false);
        var request = createCreateStationRequest();
        request.getSensors().add(createSensorWithSettingsDto(SENSOR_TSL2591, null));

        var e = catchThrowable(() -> stationValidator.validate(request));

        then(e)
            .isInstanceOf(BadRequestException.class)
            .hasMessage("Invalid sensor(s): " + SENSOR_SHT31D + ", " + SENSOR_TSL2591);
    }

    @Test
    void validateUpdateStationRequest() {
        var sensorWithSettingsDto = createSensorWithSettingsDto();
        var updateStationRequest = createUpdateStationRequestWithSensors(sensorWithSettingsDto);
        when(sensorService.sensorExistsWithName(any())).thenReturn(true);

        stationValidator.validate(updateStationRequest);

        verify(sensorService).sensorExistsWithName(SENSOR_SHT31D);
    }

    @Test
    void validateUpdateStationRequestIndoorOnly() {
        var updateStationRequest = new UpdateStationRequest().indoor(false);

        var e = catchThrowable(() -> stationValidator.validate(updateStationRequest));

        then(e).isNull();
    }

    @Test
    void validateUpdateStationRequestWithoutAnyProperty() {
        var e = catchThrowable(() -> stationValidator.validate(new UpdateStationRequest()));

        then(e).isInstanceOf(BadRequestException.class);
        then(e).hasMessage("To update a station, provide at least one property with a new value.");
    }

    @Test
    void validateUpdateStationRequestWithUnknownSensor() {
        var sensorWithSettingsDto = createSensorWithSettingsDto();
        var updateStationRequest = createUpdateStationRequestWithSensors(sensorWithSettingsDto);
        when(sensorService.sensorExistsWithName(any())).thenReturn(false);

        var e = catchThrowable(() -> stationValidator.validate(updateStationRequest));

        then(e).isInstanceOf(BadRequestException.class);
        then(e).hasMessage("Invalid sensor(s): " + SENSOR_SHT31D);
    }
}
