/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static nl.phazebroek.rcm.TestConstants.GAIN_LOW;
import static nl.phazebroek.rcm.TestConstants.GAIN_SETTING;
import static nl.phazebroek.rcm.TestConstants.METRIC_HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.METRIC_TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.SENSOR_SHT31D;
import static nl.phazebroek.rcm.TestConstants.SENSOR_TSL2591;
import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;
import static nl.phazebroek.rcm.sensor.SensorFactory.createTsl2591Sensor;
import static nl.phazebroek.rcm.settings.SensorSettingsStubFactory.createSensorSettingsDto;
import static nl.phazebroek.rcm.station.StationFactory.createStation;
import static nl.phazebroek.rcm.station.StationStubFactory.createCreateStationRequest;
import static nl.phazebroek.rcm.station.StationStubFactory.createSensorWithSettingsDto;
import static nl.phazebroek.rcm.station.StationStubFactory.createStationDto;
import static nl.phazebroek.rcm.station.StationStubFactory.createUpdateStationRequestWithSensors;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.NotFoundException;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.sensor.SensorService;
import nl.phazebroek.rcm.settings.SettingsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class StationServiceTest {

    @InjectMocks
    private StationService stationService;

    @Mock
    private StationValidator stationValidator;

    @Mock
    private StationRepository stationRepository;

    @Mock
    private StationMapper stationMapper;

    @Mock
    private SensorService sensorService;

    @Mock
    private SettingsService sensorSettingsService;

    @Captor
    private ArgumentCaptor<Station> stationCaptor;

    @Test
    void findStationByName() {
        var station = createStation();
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));

        var result = stationService.findStationByName(STATION_NAME);

        then(result).contains(station);
    }

    @Test
    void findStationByNameNotFound() {
        when(stationRepository.findStationByName(any())).thenReturn(Optional.empty());

        var result = stationService.findStationByName(STATION_NAME);

        then(result).isEmpty();
    }

    @Test
    void getSupportedMetrics() {
        var station = createStation();
        var sensor = createSht31dSensor();
        station.setSensors(List.of(sensor));

        var metrics = stationService.getSupportedMetrics(station);

        then(metrics)
            .extracting(Metric::getName)
            .containsExactlyInAnyOrder(METRIC_TEMPERATURE, METRIC_HUMIDITY);
    }

    @Test
    void addStation() {
        var station = createStation();
        when(stationMapper.toStation(any(), any())).thenReturn(station);
        when(stationRepository.save(station)).then(returnsFirstArg());
        var sht31dSensor = createSht31dSensor();
        var tsl2591Sensor = createTsl2591Sensor();
        var sensors = List.of(sht31dSensor, tsl2591Sensor);
        when(sensorService.findSensorsByName(any())).thenReturn(sensors);
        var stationDto = createStationDto();
        when(stationMapper.toStationDto(any())).thenReturn(stationDto);
        var request = createCreateStationRequest();
        request.getSensors().add(createSensorWithSettingsDto(SENSOR_TSL2591, Map.of(GAIN_SETTING, GAIN_LOW)));

        var result = stationService.addStation(request);

        then(result).isEqualTo(stationDto);
        var inOrder = inOrder(stationValidator, sensorService, stationMapper, stationRepository, sensorSettingsService);
        inOrder.verify(stationValidator).validate(request);
        inOrder.verify(sensorService).findSensorsByName(List.of(SENSOR_SHT31D, SENSOR_TSL2591));
        inOrder.verify(stationMapper).toStation(request, sensors);
        inOrder.verify(stationRepository).save(station);
        inOrder.verify(sensorSettingsService).createSettings(station, sht31dSensor, null);
        inOrder.verify(sensorSettingsService).createSettings(station, tsl2591Sensor, Map.of(GAIN_SETTING, GAIN_LOW));
        inOrder.verify(stationMapper).toStationDto(station);
    }

    @Test
    void getStations() {
        var station = createStation();
        when(stationRepository.findAll()).thenReturn(List.of(station));
        var stationDto = createStationDto();
        when(stationMapper.toStationDto(any())).thenReturn(stationDto);

        var stations = stationService.getStationDtos();

        then(stations).contains(stationDto);
        verify(stationRepository).findAll();
        verify(stationMapper).toStationDto(station);
    }

    @Test
    void updateStationSensors() {
        var station = createStation();
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));
        var sht31dSensor = createSht31dSensor();
        var tsl2591Sensor = createTsl2591Sensor();
        var sensors = List.of(sht31dSensor, tsl2591Sensor);
        when(sensorService.findSensorsByName(any())).thenReturn(sensors);
        var updateStationRequest = createUpdateStationRequestWithSensors();
        updateStationRequest.getSensors().add(createSensorWithSettingsDto(SENSOR_TSL2591, Map.of(GAIN_SETTING, GAIN_LOW)));

        stationService.updateStation(STATION_NAME, updateStationRequest);

        var inOrder = inOrder(stationRepository, stationValidator, sensorService, sensorSettingsService);
        inOrder.verify(stationRepository).findStationByName(STATION_NAME);
        inOrder.verify(stationValidator).validate(updateStationRequest);
        inOrder.verify(sensorService).findSensorsByName(List.of(SENSOR_SHT31D, SENSOR_TSL2591));
        inOrder.verify(sensorSettingsService).updateSettings(station, sht31dSensor, null);
        inOrder.verify(sensorSettingsService).updateSettings(station, tsl2591Sensor, Map.of(GAIN_SETTING, GAIN_LOW));
        inOrder.verify(stationRepository).save(stationCaptor.capture());
        then(stationCaptor.getValue().getSensors()).containsExactlyElementsOf(sensors);
    }

    @Test
    void updateStationIndoor() {
        var station = createStation();
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));

        var updateStationRequest = new UpdateStationRequest().indoor(false);

        stationService.updateStation(STATION_NAME, updateStationRequest);

        verify(stationRepository).save(stationCaptor.capture());
        var updatedStation = stationCaptor.getValue();
        then(updatedStation.isIndoor()).isFalse();
    }

    @Test
    void updateStationInvalidRequest() {
        var station = createStation();
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));
        doThrow(new BadRequestException("message")).when(stationValidator).validate(any(UpdateStationRequest.class));

        var updateStationRequest = new UpdateStationRequest().indoor(false);

        var e = catchThrowable(() -> stationService.updateStation(STATION_NAME, updateStationRequest));
        then(e).isInstanceOf(BadRequestException.class);
        then(e).hasMessage("message");
    }

    @Test
    void updateStationUnknownStation() {
        when(stationRepository.findStationByName(any())).thenReturn(Optional.empty());

        var updateStationRequest = new UpdateStationRequest().indoor(false);

        var e = catchThrowable(() -> stationService.updateStation(STATION_NAME, updateStationRequest));
        then(e).isInstanceOf(NotFoundException.class);
        then(e).hasMessage("Station " + STATION_NAME + " not found");
    }

    @Test
    void getSensorSettings() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));
        var sensorSettingsDto = createSensorSettingsDto();
        when(sensorSettingsService.getSensorSettingsDto(any(), any())).thenReturn(sensorSettingsDto);

        var result = stationService.getSensorSettingsDto(STATION_NAME, SENSOR_SHT31D);

        then(result).isEqualTo(sensorSettingsDto);
        verify(sensorSettingsService).getSensorSettingsDto(station, sensor);
    }

    @Test
    void getSensorSettingsUnknownStation() {
        when(stationRepository.findStationByName(any())).thenReturn(Optional.empty());

        var e = catchThrowable(() -> stationService.getSensorSettingsDto(STATION_NAME, SENSOR_SHT31D));
        then(e).isInstanceOf(NotFoundException.class);
        then(e).hasMessage("Station " + STATION_NAME + " not found");
    }

    @Test
    void getSensorSettingsUnknownSensor() {
        var station = createStation();
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));

        var e = catchThrowable(() -> stationService.getSensorSettingsDto(STATION_NAME, SENSOR_TSL2591));
        then(e).isInstanceOf(NotFoundException.class);
        then(e).hasMessage("Station " + STATION_NAME + " does not have sensor " + SENSOR_TSL2591);
    }

    @Test
    void saveSettings() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));
        var sensorSettingsDto = createSensorSettingsDto();

        stationService.saveSensorSettings(STATION_NAME, SENSOR_SHT31D, sensorSettingsDto);

        verify(sensorSettingsService).updateSettings(station, sensor, sensorSettingsDto.getSettings());
    }

    @Test
    void saveSettingsUnknownStation() {
        when(stationRepository.findStationByName(any())).thenReturn(Optional.empty());
        var sensorSettingsDto = createSensorSettingsDto();

        var e = catchThrowable(() -> stationService.saveSensorSettings(STATION_NAME, SENSOR_SHT31D, sensorSettingsDto));
        then(e).isInstanceOf(NotFoundException.class);
        then(e).hasMessage("Station " + STATION_NAME + " not found");
    }

    @Test
    void saveSettingsUnknownSensor() {
        var station = createStation();
        when(stationRepository.findStationByName(any())).thenReturn(Optional.of(station));
        var sensorSettingsDto = createSensorSettingsDto();

        var e = catchThrowable(() -> stationService.saveSensorSettings(STATION_NAME, SENSOR_TSL2591, sensorSettingsDto));
        then(e).isInstanceOf(NotFoundException.class);
        then(e).hasMessage("Station " + STATION_NAME + " does not have sensor " + SENSOR_TSL2591);
    }


}
