/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static nl.phazebroek.rcm.TestConstants.STATION_NAME;
import static nl.phazebroek.rcm.sensor.SensorFactory.createSht31dSensor;

import java.util.ArrayList;
import java.util.List;
import nl.phazebroek.rcm.sensor.Sensor;

public class StationFactory {

    public static Station createStation() {
        var station = new Station();
        station.setName(STATION_NAME);
        var sensors = new ArrayList<>(List.of(createSht31dSensor()));
        station.setSensors(sensors);
        station.setIndoor(true);
        return station;
    }

    public static Station createStationWithSensors(Sensor... sensors) {
        var station = createStation();
        station.setSensors(List.of(sensors));
        return station;
    }
}
