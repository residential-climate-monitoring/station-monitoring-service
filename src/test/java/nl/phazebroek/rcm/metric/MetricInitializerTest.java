/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MetricInitializerTest {

    @InjectMocks
    private MetricInitializer metricInitializer;

    @Mock
    private MetricRepository metricRepository;

    @Captor
    private ArgumentCaptor<List<Metric>> metricsCaptor;

    @Test
    void defaultMetricsCreated() {
        given(metricRepository.findAllByNameIn(any())).willReturn(emptySet());

        metricInitializer.run(null);

        verify(metricRepository).saveAll(metricsCaptor.capture());
        then(metricsCaptor.getValue()).extracting(Metric::getName).contains(DefaultMetric.TEMPERATURE.getName());
        then(metricsCaptor.getValue()).hasSize(DefaultMetric.values().length);
    }

    @Test
    void existingMetricsAreSkipped() {
        given(metricRepository.findAllByNameIn(any())).willReturn(Set.of(
            new Metric(DefaultMetric.TEMPERATURE.getName(), DefaultMetric.TEMPERATURE.getUnit())
        ));

        metricInitializer.run(null);

        verify(metricRepository).saveAll(metricsCaptor.capture());
        then(metricsCaptor.getValue()).extracting(Metric::getName).doesNotContain(DefaultMetric.TEMPERATURE.getName());
    }
}