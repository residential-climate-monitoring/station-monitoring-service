/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static nl.phazebroek.rcm.TestConstants.METRIC_HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.METRIC_ID;
import static nl.phazebroek.rcm.TestConstants.UNIT_PERCENT;
import static nl.phazebroek.rcm.metric.MetricFactory.createCreateMetricRequest;
import static nl.phazebroek.rcm.metric.MetricFactory.createHumidityMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createHumidityMetricDto;
import static nl.phazebroek.rcm.metric.MetricFactory.createTemperatureMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createTemperatureMetricDto;
import static nl.phazebroek.rcm.metric.MetricFactory.createUpdateMetricRequest;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

@ExtendWith(MockitoExtension.class)
class MetricServiceTest {

    @InjectMocks
    private MetricService metricService;

    @Mock
    private MetricRepository metricRepository;

    @Mock
    private MetricMapper metricMapper;

    @Mock
    private MetricValidator metricValidator;

    @Test
    void getMetric() {
        var metric = createTemperatureMetric();
        given(metricRepository.findById(METRIC_ID)).willReturn(Optional.of(metric));

        var result = metricService.getMetric(METRIC_ID);

        then(result).isEqualTo(metric);
    }

    @Test
    void getMetricNotFound() {
        given(metricRepository.findById(any())).willReturn(Optional.empty());

        var e = catchThrowable(() -> metricService.getMetric(METRIC_ID));
        then(e).isInstanceOf(MetricNotFoundException.class);
        then(e).hasMessageContaining(String.valueOf(METRIC_ID));
    }

    @Test
    void getMetrics() {
        var humidityMetric = createHumidityMetric();
        var temperatureMetric = createTemperatureMetric();
        var metrics = List.of(humidityMetric, temperatureMetric);
        given(metricRepository.findAll(any(Sort.class))).willReturn(metrics);
        var metricDtos = List.of(createHumidityMetricDto(), createTemperatureMetricDto());
        given(metricMapper.toMetricDto(metrics)).willReturn(metricDtos);

        var result = metricService.getMetrics();

        then(result).isEqualTo(metricDtos);
    }

    @Test
    void createMetric() {
        var request = createCreateMetricRequest();
        var metric = createTemperatureMetric();
        var metricDto = createTemperatureMetricDto();
        given(metricMapper.toMetric(request)).willReturn(metric);
        given(metricRepository.save(metric)).willReturn(metric);
        given(metricMapper.toMetricDto(metric)).willReturn(metricDto);

        var result = metricService.createMetric(request);

        then(result).isEqualTo(metricDto);
        verify(metricValidator).validate(request);
    }

    @Test
    void updateMetric() {
        var request = createUpdateMetricRequest(METRIC_HUMIDITY, UNIT_PERCENT);
        var metric = createTemperatureMetric();
        given(metricRepository.findById(METRIC_ID)).willReturn(Optional.of(metric));

        metricService.updateMetric(METRIC_ID, request);

        then(metric.getName()).isEqualTo(METRIC_HUMIDITY);
        then(metric.getUnit()).isEqualTo(UNIT_PERCENT);
        verify(metricValidator).validate(request);
        verify(metricRepository).save(metric);
    }
}