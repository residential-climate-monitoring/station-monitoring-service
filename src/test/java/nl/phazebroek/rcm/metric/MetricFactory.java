/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static nl.phazebroek.rcm.TestConstants.METRIC_HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.METRIC_IR_LIGHT;
import static nl.phazebroek.rcm.TestConstants.METRIC_LUX;
import static nl.phazebroek.rcm.TestConstants.METRIC_LUX_UNIT;
import static nl.phazebroek.rcm.TestConstants.METRIC_TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.METRIC_VISIBLE_LIGHT;
import static nl.phazebroek.rcm.TestConstants.UNIT_CELSIUS;
import static nl.phazebroek.rcm.TestConstants.UNIT_PERCENT;

import java.util.Set;
import java.util.UUID;
import nl.phazebroek.rcm.api.model.CreateMetricRequest;
import nl.phazebroek.rcm.api.model.MetricDto;
import nl.phazebroek.rcm.api.model.UpdateMetricRequest;

public class MetricFactory {

    public static Metric createMetric(String name) {
        return createMetric(name, null);
    }

    public static Metric createMetric(String name, String unit) {
        var metric = new Metric();
        metric.setName(name);
        metric.setUnit(unit);
        return metric;
    }

    public static MetricDto createMetricDto(String name, String unit) {
        return new MetricDto()
            .id(UUID.randomUUID())
            .name(name)
            .unit(unit);
    }

    public static Metric createTemperatureMetric() {
        return createMetric(METRIC_TEMPERATURE, UNIT_CELSIUS);
    }

    public static MetricDto createTemperatureMetricDto() {
        return createMetricDto(METRIC_TEMPERATURE, UNIT_CELSIUS);
    }

    public static Metric createHumidityMetric() {
        return createMetric(METRIC_HUMIDITY, UNIT_PERCENT);
    }

    public static MetricDto createHumidityMetricDto() {
        return createMetricDto(METRIC_HUMIDITY, UNIT_PERCENT);
    }

    public static Set<Metric> createSht31dMetrics() {
        return Set.of(createTemperatureMetric(), createHumidityMetric());
    }

    public static Set<Metric> createTsl2591Metrics() {
        return Set.of(
            createMetric(METRIC_LUX, METRIC_LUX_UNIT),
            createMetric(METRIC_VISIBLE_LIGHT, null),
            createMetric(METRIC_IR_LIGHT, null)
        );
    }

    public static CreateMetricRequest createCreateMetricRequest() {
        return new CreateMetricRequest()
            .name(METRIC_TEMPERATURE)
            .unit(UNIT_CELSIUS);
    }

    public static UpdateMetricRequest createUpdateMetricRequest(String name, String unit) {
        return new UpdateMetricRequest()
            .name(name)
            .unit(unit);
    }

}
