/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static nl.phazebroek.rcm.TestConstants.METRIC_TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.UNIT_CELSIUS;
import static nl.phazebroek.rcm.metric.MetricFactory.createCreateMetricRequest;
import static nl.phazebroek.rcm.metric.MetricFactory.createUpdateMetricRequest;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssertions.thenNoException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@ExtendWith(MockitoExtension.class)
class MetricValidatorTest {

    @InjectMocks
    private MetricValidator validator;

    @Mock
    private MetricRepository metricRepository;

    @Nested
    class ValidateMetrics {

        @BeforeEach
        void setUp() {
            var metric = new Metric();
            metric.setName("air-pressure");
            when(metricRepository.findAll()).thenReturn(List.of(metric));
        }

        @Test
        void metricsAreValid() {
            var metrics = List.of("air-pressure");

            thenNoException().isThrownBy(() -> validator.validate(metrics));
        }

        @ParameterizedTest
        @CsvSource({"aIr-pResSuRe", "air_pressure", "airPressure"})
        void malformedMetricsAreRejected(String metric) {
            var metrics = List.of(metric);

            var e = catchThrowableOfType(() -> validator.validate(metrics), ResponseStatusException.class);
            then(e).hasMessageContaining("Invalid metric(s): " + metric);
            then(e.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        }

        @Test
        void unknownMetricsAreRejected() {
            var metrics = List.of("swag", "alcohol");

            var e = catchThrowableOfType(() -> validator.validate(metrics), ResponseStatusException.class);
            then(e).hasMessageContaining("Invalid metric(s): alcohol, swag");
            then(e.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        }
    }

    @Nested
    class CreateMetricRequest {

        @Test
        void validateCreateMetricRequest() {
            given(metricRepository.existsByNameAndUnit(any(), any())).willReturn(false);
            var createMetricDto = createCreateMetricRequest();

            validator.validate(createMetricDto);
        }

        @Test
        void validateCreateMetricRequestNonUnique() {
            given(metricRepository.existsByNameAndUnit(any(), any())).willReturn(true);
            var createMetricDto = createCreateMetricRequest();

            var e = catchThrowableOfType(() -> validator.validate(createMetricDto), ResponseStatusException.class);
            then(e.getStatus()).isEqualTo(BAD_REQUEST);
        }
    }

    @Nested
    class UpdateMetric {

        @Test
        void validateUpdateMetricRequest() {
            var metricProperties = createUpdateMetricRequest(METRIC_TEMPERATURE, UNIT_CELSIUS);

            assertThatNoException().isThrownBy(() -> validator.validate(metricProperties));
        }

        @Test
        void validateUpdateMetricRequestWithNameOnly() {
            var metricProperties = createUpdateMetricRequest(METRIC_TEMPERATURE, null);

            assertThatNoException().isThrownBy(() -> validator.validate(metricProperties));
        }

        @Test
        void validateUpdateMetricRequestWithUnitOnly() {
            var metricProperties = createUpdateMetricRequest(null, UNIT_CELSIUS);

            assertThatNoException().isThrownBy(() -> validator.validate(metricProperties));
        }

        @Test
        void validateEmptyUpdateMetricRequest() {
            var metricProperties = createUpdateMetricRequest(null, null);

            var e = catchThrowableOfType(() -> validator.validate(metricProperties), ResponseStatusException.class);
            then(e.getStatus()).isEqualTo(BAD_REQUEST);
        }

    }
}