/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static nl.phazebroek.rcm.TestConstants.ADMIN;
import static nl.phazebroek.rcm.TestConstants.METRIC_HUMIDITY;
import static nl.phazebroek.rcm.TestConstants.METRIC_TEMPERATURE;
import static nl.phazebroek.rcm.TestConstants.UNIT_CELSIUS;
import static nl.phazebroek.rcm.TestConstants.UNIT_FAHRENHEIT;
import static nl.phazebroek.rcm.TestConstants.UNIT_PERCENT;
import static nl.phazebroek.rcm.metric.MetricFactory.createCreateMetricRequest;
import static nl.phazebroek.rcm.metric.MetricFactory.createHumidityMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createTemperatureMetric;
import static nl.phazebroek.rcm.metric.MetricFactory.createUpdateMetricRequest;
import static org.assertj.core.api.BDDAssertions.then;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;
import lombok.SneakyThrows;
import nl.phazebroek.rcm.BaseIT;
import nl.phazebroek.rcm.api.model.CreateMetricRequest;
import nl.phazebroek.rcm.api.model.GetMetricsResponse;
import nl.phazebroek.rcm.api.model.MetricDto;
import nl.phazebroek.rcm.api.model.UpdateMetricRequest;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.ResultActions;

public class MetricControllerIT extends BaseIT {

    @Nested
    class GetMetrics {

        @Test
        @WithMockUser
        @SneakyThrows
        void getMetricsEmpty() {
            getMetrics()
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.metrics").isEmpty());
        }

        @Test
        @WithMockUser
        @SneakyThrows
        void getMetricsSortedByNameAscending() {
            metricRepository.saveAll(List.of(createTemperatureMetric(), createHumidityMetric()));

            var content = getMetrics()
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            var response = objectMapper.readValue(content, GetMetricsResponse.class);

            then(response.getMetrics()).hasSize(2);
            then(response.getMetrics()).extracting(MetricDto::getName).containsExactly(METRIC_HUMIDITY, METRIC_TEMPERATURE);
        }

        @SneakyThrows
        private ResultActions getMetrics() {
            return mockMvc.perform(get("/metrics"));
        }
    }

    @Nested
    class PostMetric {

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void postMetric() {
            var request = createCreateMetricRequest();

            var content = postMetric(request)
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            var metricDto = objectMapper.readValue(content, MetricDto.class);

            then(metricDto.getId()).isNotNull();
            then(metricDto.getName()).isEqualTo(METRIC_TEMPERATURE);
            then(metricDto.getUnit()).isEqualTo(UNIT_CELSIUS);
            var metric = metricRepository.findById(metricDto.getId()).orElse(null);
            then(metric)
                .extracting(Metric::getName, Metric::getUnit)
                .containsExactly(METRIC_TEMPERATURE, UNIT_CELSIUS);
        }

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void postMetricWithUniqueUnit() {
            var metric = createMetric(METRIC_TEMPERATURE, UNIT_FAHRENHEIT);
            metricRepository.save(metric);

            var request = createCreateMetricRequest();

            postMetric(request)
                .andExpect(status().isOk());
        }

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void cannotPostDuplicateMetric() {
            var metric = createTemperatureMetric();
            metricRepository.save(metric);

            var request = createCreateMetricRequest();

            postMetric(request)
                .andExpect(status().isBadRequest());
        }

        @Test
        @WithAnonymousUser
        void createMetricUnauthorized() throws Exception {
            var request = createCreateMetricRequest();

            postMetric(request)
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void createMetricForbidden() throws Exception {
            var request = createCreateMetricRequest();

            postMetric(request)
                .andExpect(status().isForbidden());
        }

        @SneakyThrows
        private ResultActions postMetric(CreateMetricRequest request) {
            return mockMvc.perform(post("/metrics")
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }

    @Nested
    class PatchMetric {

        private UUID setUpMetric() {
            var metric = createTemperatureMetric();
            metric = metricRepository.save(metric);
            return metric.getId();
        }

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void changeMetricUnit() {
            var metricId = setUpMetric();
            var request = createUpdateMetricRequest(null, UNIT_FAHRENHEIT);

            patchMetric(metricId, request).andExpect(status().isNoContent());

            var metric = metricRepository.findById(metricId).orElse(new Metric());
            then(metric.getUnit()).isEqualTo(UNIT_FAHRENHEIT);
        }

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void changeMetricName() {
            var metricId = setUpMetric();
            var request = createUpdateMetricRequest("foo", null);

            patchMetric(metricId, request).andExpect(status().isNoContent());

            var metric = metricRepository.findById(metricId).orElse(new Metric());
            then(metric.getName()).isEqualTo("foo");
        }

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void updateMetricStaysUnique() {
            metricRepository.save(createHumidityMetric());
            var metricId = setUpMetric();
            var request = createUpdateMetricRequest(METRIC_HUMIDITY, UNIT_PERCENT);

            patchMetric(metricId, request).andExpect(status().isBadRequest());
        }

        @Test
        @SneakyThrows
        @WithMockUser(roles = ADMIN)
        void patchWithoutProperties() {
            var metricId = setUpMetric();
            var request = createUpdateMetricRequest(null, null);

            patchMetric(metricId, request).andExpect(status().isBadRequest());
        }

        @Test
        @WithAnonymousUser
        void createMetricUnauthorized() throws Exception {
            var request = createUpdateMetricRequest("foo", null);

            patchMetric(UUID.randomUUID(), request)
                .andExpect(status().isUnauthorized());
        }

        @Test
        @WithMockUser
        void createMetricForbidden() throws Exception {
            var request = createUpdateMetricRequest("foo", null);

            patchMetric(UUID.randomUUID(), request)
                .andExpect(status().isForbidden());
        }

        @SneakyThrows
        private ResultActions patchMetric(UUID id, UpdateMetricRequest request) {
            return mockMvc.perform(patch("/metrics/{id}", id)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .with(csrf())
            );
        }
    }
}
