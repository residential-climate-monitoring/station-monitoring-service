/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static nl.phazebroek.rcm.TestConstants.HUE_MOTION_SENSOR_NAME_1;
import static nl.phazebroek.rcm.TestConstants.LIGHT_VAL;
import static nl.phazebroek.rcm.TestConstants.MOTION_SENSOR_MODEL_ID;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createLightSensor;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createPresenceSensor;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createTemperatureSensor;
import static org.assertj.core.api.BDDAssertions.then;

import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HueMapperTest {

    @InjectMocks
    private HueMapperImpl hueMapper;

    private HuePresenceSensor presenceSensor;
    private HueLightLevelSensor lightSensor;
    private HueTemperatureSensor temperatureSensor;

    @BeforeEach
    void setUp() {
        presenceSensor = createPresenceSensor();
        lightSensor = createLightSensor();
        temperatureSensor = createTemperatureSensor();
    }

    @Test
    void toMotionSensor() {
        var motionSensor = hueMapper.toMotionSensor(presenceSensor, lightSensor, temperatureSensor);

        then(motionSensor.getName()).isEqualTo(HUE_MOTION_SENSOR_NAME_1);
        then(motionSensor.isOn()).isTrue();
        then(motionSensor.isReachable()).isTrue();
        then(motionSensor.isPresence()).isTrue();
        then(motionSensor.getModelId()).isEqualTo(MOTION_SENSOR_MODEL_ID);
        then(motionSensor.isDark()).isFalse();
        then(motionSensor.isDaylight()).isTrue();
        then(motionSensor.getLightLevel()).isEqualTo(LIGHT_VAL);
        then(motionSensor.getTemperature()).isEqualTo(TEMPERATURE_VAL);
    }

    @Test
    void toMotionSensorNoLastUpdated() {
        presenceSensor.getState().setLastUpdated("none");

        var motionSensor = hueMapper.toMotionSensor(presenceSensor, lightSensor, temperatureSensor);

        then(motionSensor.getLastUpdated()).isNull();
    }
}