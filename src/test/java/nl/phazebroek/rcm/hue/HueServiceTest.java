/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static nl.phazebroek.rcm.TestConstants.BATTERY_PERCENTAGE;
import static nl.phazebroek.rcm.TestConstants.HUE_COMMON_UNIQUE_ID_2;
import static nl.phazebroek.rcm.TestConstants.HUE_CONFIG_MOTION_SENSOR_NAME;
import static nl.phazebroek.rcm.TestConstants.HUE_MOTION_SENSOR_NAME_1;
import static nl.phazebroek.rcm.TestConstants.HUE_MOTION_SENSOR_NAME_2;
import static nl.phazebroek.rcm.TestConstants.LIGHT_VAL;
import static nl.phazebroek.rcm.TestConstants.OUTDOOR_SENSOR_MODEL_ID;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createLightSensor;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createPresenceSensor;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createSimpleSensor;
import static nl.phazebroek.rcm.hue.HueSensorFactory.createTemperatureSensor;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.hue.api.HueSensorDto;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;
import nl.phazebroek.rcm.hue.model.HueMotionSensor;
import nl.phazebroek.rcm.measurement.MeasurementService;
import nl.phazebroek.rcm.station.StationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class HueServiceTest {

    @Captor
    private ArgumentCaptor<MeasurementReportDto> measurementReportCaptor;

    @InjectMocks
    private HueService hueService;

    @Mock
    private HueClient hueClient;

    @Mock
    private MeasurementService measurementService;

    @Mock
    private HueMapper hueMapper;

    @Mock
    private StationService stationService;

    @Mock
    private HueConfiguration hueConfiguration;

    private HuePresenceSensor presenceSensor;
    private HueLightLevelSensor lightSensor;
    private HueTemperatureSensor temperatureSensor;
    private List<HueSensorDto> hueSensors;

    @BeforeEach
    void setUp() {
        presenceSensor = createPresenceSensor();
        lightSensor = createLightSensor();
        temperatureSensor = createTemperatureSensor();
        hueSensors = new ArrayList<>(List.of(
            createSimpleSensor(),
            presenceSensor,
            lightSensor,
            temperatureSensor
        ));
        given(hueClient.getSensors()).willReturn(Mono.just(hueSensors));
    }

    private void setupMotionSensor() {
        var motionSensor = new MotionSensorBuilder().withDefaults().build();
        setupMotionSensor(motionSensor);
    }

    private void setupMotionSensor(HueMotionSensor motionSensor) {
        given(hueMapper.toMotionSensor(presenceSensor, lightSensor, temperatureSensor)).willReturn(motionSensor);
    }

    private void setupAdditionalMotionSensor() {
        var presenceSensor2 = createPresenceSensor(HUE_MOTION_SENSOR_NAME_2, HUE_COMMON_UNIQUE_ID_2);
        var lightSensor2 = createLightSensor(HUE_MOTION_SENSOR_NAME_2, HUE_COMMON_UNIQUE_ID_2);
        var temperatureSensor2 = createTemperatureSensor(HUE_MOTION_SENSOR_NAME_2, HUE_COMMON_UNIQUE_ID_2);
        hueSensors.addAll(List.of(presenceSensor2, lightSensor2, temperatureSensor2));
        var motionSensor2 = new MotionSensorBuilder().withDefaults().name(HUE_MOTION_SENSOR_NAME_2).build();
        given(hueMapper.toMotionSensor(presenceSensor2, lightSensor2, temperatureSensor2)).willReturn(motionSensor2);
    }

    @Test
    void measurementReportGeneratedForOneMotionSensor() {
        setupMotionSensor();

        hueService.monitorSensors();

        verify(measurementService).processMeasurementReport(measurementReportCaptor.capture());
        var measurementReport = measurementReportCaptor.getValue();
        then(measurementReport.getStationName()).isEqualTo(HUE_MOTION_SENSOR_NAME_1);
        var measurements = measurementReport.getMeasurements();
        then(measurements.get("presence")).isEqualTo(1.0);
        then(measurements.get("lux")).isEqualTo(LIGHT_VAL);
        then(measurements.get("temperature")).isEqualTo(TEMPERATURE_VAL);
        then(measurements.get("battery")).isEqualTo(BATTERY_PERCENTAGE);
    }

    @Test
    void measurementReportGeneratedForMultipleMotionSensors() {
        setupMotionSensor();
        setupAdditionalMotionSensor();

        hueService.monitorSensors();

        verify(measurementService, times(2)).processMeasurementReport(measurementReportCaptor.capture());
        then(measurementReportCaptor.getAllValues())
            .extracting(MeasurementReportDto::getStationName)
            .containsExactlyInAnyOrder(HUE_MOTION_SENSOR_NAME_1, HUE_MOTION_SENSOR_NAME_2);
    }

    @Test
    void hueSensorsMustShareCommonUniqueId() {
        lightSensor.setUniqueId(HUE_COMMON_UNIQUE_ID_2);

        hueService.monitorSensors();

        verifyNoInteractions(measurementService);
    }

    @Test
    void motionSensorIsReportedWithNoMotion() {
        setupMotionSensor(new MotionSensorBuilder()
            .withDefaults()
            .presence(false)
            .lastUpdate(LocalDateTime.now(UTC).minus(1, HOURS))
            .build());

        hueService.monitorSensors();

        verify(measurementService).processMeasurementReport(measurementReportCaptor.capture());
        then(measurementReportCaptor.getValue().getMeasurements().get("presence")).isEqualTo(0.0);
    }

    /**
     * Ensures that motion is reported when the presence sensor detected motion since the last scheduled interval, even when it currently
     * does not detect any motion. This is because the presence sensor state might change more frequently than the schedule interval
     * frequency.
     */
    @Test
    void motionDetectionCorrectedWithinScheduleInterval() {
        var state = presenceSensor.getState();
        state.setPresence(false);
        state.setLastUpdated(LocalDateTime.now(UTC).minus(59, SECONDS).toString());

        hueService.monitorSensors();

        var presenceSensorCaptor = ArgumentCaptor.forClass(HuePresenceSensor.class);
        verify(hueMapper).toMotionSensor(presenceSensorCaptor.capture(), any(), any());
        then(presenceSensorCaptor.getValue().getState().isPresence()).isTrue();
    }

    @Test
    void motionDetectionNotCorrectedOutsideScheduleInterval() {
        var state = presenceSensor.getState();
        state.setPresence(false);
        state.setLastUpdated(LocalDateTime.now(UTC).minus(61, SECONDS).toString());

        hueService.monitorSensors();

        var presenceSensorCaptor = ArgumentCaptor.forClass(HuePresenceSensor.class);
        verify(hueMapper).toMotionSensor(presenceSensorCaptor.capture(), any(), any());
        then(presenceSensorCaptor.getValue().getState().isPresence()).isFalse();
    }

    /**
     * When motion sensors are just initialized, they will yield "none" if they have no updates yet.
     */
    @Test
    void motionDetectionSensorWithoutLastUpdatedTimestamp() {
        var state = presenceSensor.getState();
        state.setPresence(false);
        state.setLastUpdated("none");

        hueService.monitorSensors();

        var presenceSensorCaptor = ArgumentCaptor.forClass(HuePresenceSensor.class);
        verify(hueMapper).toMotionSensor(presenceSensorCaptor.capture(), any(), any());
        then(presenceSensorCaptor.getValue().getState().isPresence()).isFalse();
    }

    @Test
    void hueClientExceptionHandled() {
        given(hueClient.getSensors()).willReturn(Mono.error(new RuntimeException("some error")));

        assertThatNoException().isThrownBy(() -> hueService.monitorSensors());
    }

    @Test
    void stationCreatedAutomatically() {
        setupMotionSensor();
        given(hueConfiguration.getMotionSensorName()).willReturn(HUE_CONFIG_MOTION_SENSOR_NAME);

        hueService.monitorSensors();

        var requestCaptor = ArgumentCaptor.forClass(CreateStationRequest.class);
        verify(stationService).addStation(requestCaptor.capture());
        var request = requestCaptor.getValue();
        then(request.getName()).isEqualTo(HUE_MOTION_SENSOR_NAME_1);
        then(request.getIndoor()).isTrue();
        then(request.getSensors()).extracting(SensorWithSettingsDto::getName).containsExactly(HUE_CONFIG_MOTION_SENSOR_NAME);
    }

    @Test
    void stationsCreatedAutomatically() {
        setupMotionSensor();
        setupAdditionalMotionSensor();

        hueService.monitorSensors();

        var requestCaptor = ArgumentCaptor.forClass(CreateStationRequest.class);
        verify(stationService, times(2)).addStation(requestCaptor.capture());
        then(requestCaptor.getAllValues()).extracting(CreateStationRequest::getName)
            .containsExactlyInAnyOrder(HUE_MOTION_SENSOR_NAME_1, HUE_MOTION_SENSOR_NAME_2);
    }

    @Test
    void outdoorStationCreatedAutomatically() {
        var motionSensor = new MotionSensorBuilder().withDefaults().modelId(OUTDOOR_SENSOR_MODEL_ID).build();
        given(hueMapper.toMotionSensor(presenceSensor, lightSensor, temperatureSensor)).willReturn(motionSensor);

        hueService.monitorSensors();

        verify(stationService).addStation(argThat(request -> !request.getIndoor()));
    }

}