/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static nl.phazebroek.rcm.TestConstants.HUE_CONFIG_MOTION_SENSOR_NAME;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.sensor.SensorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HueSensorInitializerTest {

    @InjectMocks
    private HueSensorInitializer hueSensorInitializer;

    @Mock
    private SensorService sensorService;

    @Mock
    private HueConfiguration hueConfiguration;

    @BeforeEach
    void setUp() {
        given(hueConfiguration.getMotionSensorName()).willReturn(HUE_CONFIG_MOTION_SENSOR_NAME);
    }

    @Test
    void hueSensorInitializedWhenNonExistent() {
        given(sensorService.sensorExistsWithName(HUE_CONFIG_MOTION_SENSOR_NAME)).willReturn(false);

        hueSensorInitializer.run(null);

        var createSensorDtoCaptor = ArgumentCaptor.forClass(CreateSensorDto.class);
        verify(sensorService).addSensor(createSensorDtoCaptor.capture());
        var createSensorDto = createSensorDtoCaptor.getValue();
        then(createSensorDto.getName()).isEqualTo(HUE_CONFIG_MOTION_SENSOR_NAME);
        then(createSensorDto.getMetrics()).containsExactlyInAnyOrder("presence", "temperature", "lux", "battery");
    }

    @Test
    void hueSensorInitializationSkippedWhenExisting() {
        given(sensorService.sensorExistsWithName(HUE_CONFIG_MOTION_SENSOR_NAME)).willReturn(true);

        hueSensorInitializer.run(null);

        verify(sensorService, never()).addSensor(any());
    }
}