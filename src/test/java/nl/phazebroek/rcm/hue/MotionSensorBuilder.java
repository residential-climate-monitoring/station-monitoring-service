/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static java.time.ZoneOffset.UTC;
import static nl.phazebroek.rcm.TestConstants.BATTERY_PERCENTAGE;
import static nl.phazebroek.rcm.TestConstants.HUE_MOTION_SENSOR_NAME_1;
import static nl.phazebroek.rcm.TestConstants.LIGHT_VAL;
import static nl.phazebroek.rcm.TestConstants.MOTION_SENSOR_MODEL_ID;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;

import java.time.LocalDateTime;
import nl.phazebroek.rcm.hue.model.HueMotionSensor;

public class MotionSensorBuilder {

    private final HueMotionSensor sensor;

    public MotionSensorBuilder() {
        sensor = new HueMotionSensor();
    }

    public MotionSensorBuilder withDefaults() {
        name(HUE_MOTION_SENSOR_NAME_1);
        presence(true);
        lightLevel(LIGHT_VAL);
        temperature(TEMPERATURE_VAL);
        battery(BATTERY_PERCENTAGE);
        lastUpdate(LocalDateTime.now(UTC));
        modelId(MOTION_SENSOR_MODEL_ID);
        return this;
    }

    public MotionSensorBuilder name(String name) {
        sensor.setName(name);
        return this;
    }

    public MotionSensorBuilder presence(boolean presence) {
        sensor.setPresence(presence);
        return this;
    }

    public MotionSensorBuilder lightLevel(int lightLevel) {
        sensor.setLightLevel(lightLevel);
        return this;
    }

    public MotionSensorBuilder temperature(double temperature) {
        sensor.setTemperature(temperature);
        return this;
    }

    public MotionSensorBuilder battery(double battery) {
        sensor.setBattery(battery);
        return this;
    }

    public MotionSensorBuilder lastUpdate(LocalDateTime lastUpdated) {
        sensor.setLastUpdated(lastUpdated);
        return this;
    }

    public MotionSensorBuilder modelId(String modelId) {
        sensor.setModelId(modelId);
        return this;
    }

    public HueMotionSensor build() {
        return sensor;
    }

}
