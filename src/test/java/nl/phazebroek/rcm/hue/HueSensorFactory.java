/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static java.time.ZoneOffset.UTC;
import static nl.phazebroek.rcm.TestConstants.BATTERY_PERCENTAGE;
import static nl.phazebroek.rcm.TestConstants.HUE_COMMON_UNIQUE_ID_1;
import static nl.phazebroek.rcm.TestConstants.HUE_MOTION_SENSOR_NAME_1;
import static nl.phazebroek.rcm.TestConstants.LIGHT_VAL;
import static nl.phazebroek.rcm.TestConstants.MOTION_SENSOR_MODEL_ID;
import static nl.phazebroek.rcm.TestConstants.TEMPERATURE_VAL;

import java.time.LocalDateTime;
import nl.phazebroek.rcm.hue.api.HueConfigDto;
import nl.phazebroek.rcm.hue.api.HueSensorDto;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;
import nl.phazebroek.rcm.hue.api.HueStateDto;

public class HueSensorFactory {

    public static HueSensorDto createSimpleSensor() {
        var sensor = new HueSensorDto();
        sensor.setName("Daylight");
        sensor.setConfig(createSimpleHueConfig());
        var state = createHueState();
        state.setDaylight(true);
        sensor.setState(state);
        return sensor;
    }

    public static HuePresenceSensor createPresenceSensor() {
        return createPresenceSensor(HUE_MOTION_SENSOR_NAME_1, HUE_COMMON_UNIQUE_ID_1);
    }

    public static HuePresenceSensor createPresenceSensor(String name, String commonUniqueId) {
        var sensor = new HuePresenceSensor();
        sensor.setName(name);
        sensor.setUniqueId(commonUniqueId + "-00-001");
        sensor.setModelId(MOTION_SENSOR_MODEL_ID);
        var state = createHueState();
        state.setPresence(true);
        sensor.setState(state);
        sensor.setConfig(createHueConfig());
        return sensor;
    }

    public static HueLightLevelSensor createLightSensor() {
        return createLightSensor(HUE_MOTION_SENSOR_NAME_1, HUE_COMMON_UNIQUE_ID_1);
    }

    public static HueLightLevelSensor createLightSensor(String name, String commonUniqueId) {
        var sensor = new HueLightLevelSensor();
        sensor.setName(name);
        sensor.setUniqueId(commonUniqueId + "-00-002");
        var state = createHueState();
        state.setLightLevel(LIGHT_VAL);
        state.setDaylight(true);
        sensor.setState(state);
        sensor.setConfig(createHueConfig());
        return sensor;
    }

    public static HueTemperatureSensor createTemperatureSensor() {
        return createTemperatureSensor(HUE_MOTION_SENSOR_NAME_1, HUE_COMMON_UNIQUE_ID_1);
    }

    public static HueTemperatureSensor createTemperatureSensor(String name, String commonUniqueId) {
        var sensor = new HueTemperatureSensor();
        sensor.setName(name);
        sensor.setUniqueId(commonUniqueId + "-00-003");
        var state = createHueState();
        state.setTemperature((int) (TEMPERATURE_VAL * 100));
        sensor.setState(state);
        sensor.setConfig(createHueConfig());
        return sensor;
    }

    private static HueStateDto createHueState() {
        var state = new HueStateDto();
        state.setLastUpdated(LocalDateTime.now(UTC).toString());
        return state;
    }

    private static HueConfigDto createSimpleHueConfig() {
        var config = new HueConfigDto();
        config.setOn(true);
        return config;
    }

    private static HueConfigDto createHueConfig() {
        var config = createSimpleHueConfig();
        config.setBattery((int) (BATTERY_PERCENTAGE * 100));
        config.setReachable(true);
        config.setOn(true);
        return config;
    }
}
