/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.assertj.core.api.BDDAssertions.then;

import java.time.Duration;
import nl.phazebroek.rcm.WiremockIT;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueDaylightSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueGeofenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

@ActiveProfiles("hue-integration")
class HueClientIT extends WiremockIT {

    @Autowired
    private HueClient hueClient;

    @Test
    void getSensorsResponseIsMappedToAllSubTypes() {
        stubFor(get(urlEqualTo("/api/username/sensors"))
            .willReturn(aResponseWithBodyFile("hue-get-sensors-response.json")));

        var mono = hueClient.getSensors();

        StepVerifier.create(mono)
            .assertNext(sensors -> {
                then(sensors).hasSize(5);
                then(sensors.get(0)).isInstanceOf(HueDaylightSensor.class);
                then(sensors.get(1)).isInstanceOf(HuePresenceSensor.class);
                then(sensors.get(2)).isInstanceOf(HueLightLevelSensor.class);
                then(sensors.get(3)).isInstanceOf(HueTemperatureSensor.class);
                then(sensors.get(4)).isInstanceOf(HueGeofenceSensor.class);
            })
            .expectComplete()
            .verify(Duration.ofSeconds(3));

        verify(getRequestedFor(urlEqualTo("/api/username/sensors")));
    }

    @Test
    void getSensorsFailed() {
        stubFor(get(urlEqualTo("/api/username/sensors"))
            .willReturn(a5xxResponse()));

        var mono = hueClient.getSensors();

        StepVerifier.create(mono)
            .expectError()
            .verify(Duration.ofSeconds(3));

        verify(getRequestedFor(urlEqualTo("/api/username/sensors")));
    }

}