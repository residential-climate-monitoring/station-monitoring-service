/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm;

import static nl.phazebroek.rcm.settings.SensorSettingsStubFactory.createSettings;
import static nl.phazebroek.rcm.station.StationFactory.createStation;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.phazebroek.rcm.metric.MetricRepository;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.sensor.SensorRepository;
import nl.phazebroek.rcm.settings.SettingsRepository;
import nl.phazebroek.rcm.station.Station;
import nl.phazebroek.rcm.station.StationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Base class for integration tests.
 */
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("integration")
public abstract class BaseIT {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected SensorRepository sensorRepository;

    @Autowired
    protected StationRepository stationRepository;

    @Autowired
    protected SettingsRepository sensorSettingsRepository;

    @Autowired
    protected MetricRepository metricRepository;

    @BeforeEach
    void setUp() {
        sensorSettingsRepository.deleteAll();
        stationRepository.deleteAll();
        sensorRepository.deleteAll();
        metricRepository.deleteAll();
    }

    protected Station setupDefaultStation() {
        var station = createStation();
        station.getSensors().forEach(this::setupSensor);
        var savedStation = stationRepository.save(station);
        var sensors = station.getSensors();
        sensors.forEach(sensor -> {
            var sensorSettings = createSettings(savedStation, sensor);
            sensorSettingsRepository.save(sensorSettings);
        });
        return station;
    }

    protected Sensor setupSensor(Sensor sensor) {
        metricRepository.saveAll(sensor.getMetrics());
        return sensorRepository.save(sensor);
    }

}
