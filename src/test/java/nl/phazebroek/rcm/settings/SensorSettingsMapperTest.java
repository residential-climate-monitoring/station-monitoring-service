/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import static nl.phazebroek.rcm.TestConstants.GAIN_LOW;
import static nl.phazebroek.rcm.TestConstants.GAIN_SETTING;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;

import java.util.Map;
import nl.phazebroek.rcm.api.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SensorSettingsMapperTest {

    @InjectMocks
    private SettingsMapperImpl sensorSettingsMapper;

    @Test
    void toSensorSettingsDto() {
        var target = sensorSettingsMapper.toSensorSettingsDto(Map.of(GAIN_SETTING, GAIN_LOW));

        then(target.getSettings()).containsExactlyEntriesOf(Map.of(GAIN_SETTING, GAIN_LOW));
    }

    @Test
    void toMapWithStrings() {
        var target = sensorSettingsMapper.toMapWithStrings(Map.of(GAIN_SETTING, GAIN_LOW));

        then(target).containsExactlyEntriesOf(Map.of(GAIN_SETTING, GAIN_LOW));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void toMapWithStringsEmptySource(Map<String, Object> source) {
        var result = sensorSettingsMapper.toMapWithStrings(source);

        then(result)
            .isNotNull()
            .isEmpty();
    }

    @Test
    void toMapWithStringsIncludingNonStrings() {
        var e = catchThrowable(() -> sensorSettingsMapper.toMapWithStrings(Map.of("key", true)));

        then(e)
            .isInstanceOf(BadRequestException.class)
            .hasMessage("All values must be strings.");
    }
}