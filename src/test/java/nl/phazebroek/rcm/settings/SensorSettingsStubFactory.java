/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import static nl.phazebroek.rcm.TestConstants.GAIN_LOW;
import static nl.phazebroek.rcm.TestConstants.GAIN_SETTING;

import java.util.HashMap;
import java.util.Map;
import nl.phazebroek.rcm.api.model.SensorSettingsDto;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.station.Station;

public abstract class SensorSettingsStubFactory {

    public static Settings createSettings(Station station, Sensor sensor, Map<String, String> settings) {
        var sensorSettings = new Settings();
        sensorSettings.setStation(station);
        sensorSettings.setSensor(sensor);
        sensorSettings.setSettings(settings);
        return sensorSettings;
    }

    public static Settings createSettings(Station station, Sensor sensor) {
        return createSettings(station, sensor, new HashMap<>());
    }

    public static SensorSettingsDto createSensorSettingsDto() {
        return new SensorSettingsDto()
            .settings(Map.of(GAIN_SETTING, GAIN_LOW));
    }

}
