/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import static java.util.Collections.emptyMap;
import static nl.phazebroek.rcm.TestConstants.GAIN_LOW;
import static nl.phazebroek.rcm.TestConstants.GAIN_MEDIUM;
import static nl.phazebroek.rcm.TestConstants.GAIN_SETTING;
import static nl.phazebroek.rcm.settings.SensorSettingsStubFactory.createSensorSettingsDto;
import static nl.phazebroek.rcm.settings.SensorSettingsStubFactory.createSettings;
import static nl.phazebroek.rcm.station.StationFactory.createStation;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SensorSettingsServiceTest {

    @InjectMocks
    private SettingsService settingsService;

    @Mock
    private SettingsRepository settingsRepository;

    @Mock
    private SettingsMapper settingsMapper;

    @Test
    void getSensorSettingsDto() {
        var station = createStation();
        var settings = Map.of(GAIN_SETTING, GAIN_LOW);
        var sensor = station.getSensors().get(0);
        var sensorSettings = createSettings(station, sensor, settings);
        given(settingsRepository.findByStationAndSensor(any(), any())).willReturn(Optional.of(sensorSettings));
        var expectedSensorSettingsDto = createSensorSettingsDto();
        given(settingsMapper.toSensorSettingsDto(any())).willReturn(expectedSensorSettingsDto);

        var actualSensorSettingsDto = settingsService.getSensorSettingsDto(station, sensor);

        then(actualSensorSettingsDto).isEqualTo(expectedSensorSettingsDto);
        verify(settingsRepository).findByStationAndSensor(station, sensor);
        verify(settingsMapper).toSensorSettingsDto(settings);
    }

    @Test
    void getSensorSettingsNotFound() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        given(settingsRepository.findByStationAndSensor(any(), any())).willReturn(Optional.empty());
        var sensorSettings = createSettings(station, sensor);
        when(settingsMapper.toSensorSettings(any(), any(), any())).thenReturn(sensorSettings);
        when(settingsRepository.save(any())).then(returnsFirstArg());
        var sensorSettingsDto = createSensorSettingsDto();
        given(settingsMapper.toSensorSettingsDto(any())).willReturn(sensorSettingsDto);

        var result = settingsService.getSensorSettingsDto(station, sensor);

        then(result).isEqualTo(sensorSettingsDto);
        verify(settingsMapper).toSensorSettings(station, sensor, null);
        verify(settingsRepository).save(sensorSettings);
    }

    @Test
    void createNewSensorSettings() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        Map<String, Object> settings = Map.of(GAIN_SETTING, GAIN_LOW);
        var sensorSettings = createSettings(station, sensor);
        when(settingsMapper.toSensorSettings(any(), any(), any())).thenReturn(sensorSettings);
        when(settingsRepository.save(any())).then(returnsFirstArg());

        var result = settingsService.createSettings(station, sensor, settings);

        then(result).isEqualTo(sensorSettings);
        verify(settingsMapper).toSensorSettings(station, sensor, settings);
        verify(settingsRepository).save(sensorSettings);
    }

    @Test
    void updateSensorSettingsOverwriteExistingSettings() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        var sensorSettings = createSettings(station, sensor);
        when(settingsRepository.findByStationAndSensor(any(), any())).thenReturn(Optional.of(sensorSettings));
        Map<String, Object> settings = Map.of(GAIN_SETTING, GAIN_MEDIUM);
        Map<String, String> settingsAsStrings = Map.of(GAIN_SETTING, GAIN_MEDIUM);
        when(settingsMapper.toMapWithStrings(any())).thenReturn(settingsAsStrings);

        settingsService.updateSettings(station, sensor, settings);

        then(sensorSettings.getSettings()).containsExactlyEntriesOf(settingsAsStrings);
        verify(settingsMapper).toMapWithStrings(settings);
        verify(settingsRepository).save(sensorSettings);
    }

    @Test
    void updateSensorSettingsNoExistingSettings() {
        var station = createStation();
        var sensor = station.getSensors().get(0);
        var sensorSettings = createSettings(station, sensor);
        when(settingsRepository.findByStationAndSensor(any(), any())).thenReturn(Optional.empty());
        when(settingsMapper.toSensorSettings(any(), any(), any())).thenReturn(sensorSettings);
        Map<String, Object> settings = Map.of(GAIN_SETTING, GAIN_MEDIUM);
        Map<String, String> settingsAsStrings = Map.of(GAIN_SETTING, GAIN_MEDIUM);
        when(settingsMapper.toMapWithStrings(any())).thenReturn(settingsAsStrings);

        settingsService.updateSettings(station, sensor, settings);

        then(sensorSettings.getSettings()).containsExactlyEntriesOf(settingsAsStrings);
        verify(settingsMapper).toSensorSettings(station, sensor, emptyMap());
        verify(settingsMapper).toMapWithStrings(settings);
        verify(settingsRepository).save(sensorSettings);
    }
}