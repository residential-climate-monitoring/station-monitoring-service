/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.user;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import nl.phazebroek.rcm.BaseIT;
import org.junit.jupiter.api.Test;
import org.springframework.security.test.context.support.WithMockUser;

class UserControllerIT extends BaseIT {

    @Test
    @WithMockUser
    void getUser() throws Exception {
        mockMvc.perform(get("/user"))
            .andExpect(status().isOk());
    }

}
