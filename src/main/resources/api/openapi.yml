openapi: 3.0.3
info:
  version: "1.0"
  title: Sensor API
  license:
    name: MIT
    url: https://opensource.org/licenses/MIT
servers:
  - url: https://gitlab.com/residential-climate-monitoring/station-monitoring-service
    description: Project root
paths:
  /stations:
    get:
      operationId: getStations
      description: "Get a list of all the stations that are being monitored."
      tags:
        - stations
      responses:
        200:
          description: "List of all stations. Can be empty."
          content:
            application/json:
              example: "./examples/stations/get-stations-response.json"
              schema:
                $ref: "./schemas/station.yml#/GetStationsResponse"
    post:
      operationId: postStation
      description: "Add a station to be monitored by the service."
      tags:
        - stations
      requestBody:
        required: true
        content:
          application/json:
            examples:
              simple:
                $ref: "./examples/stations/create-station-request-simple.json"
              full:
                $ref: "./examples/stations/create-station-request-full.json"
            schema:
              $ref: "./schemas/station.yml#/CreateStationRequest"
      responses:
        200:
          description: "Station added successfully"
          content:
            application/json:
              example: "./examples/stations/station-dto.json"
              schema:
                $ref: "./schemas/station.yml#/StationDto"
        400:
          description: "Invalid station"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"

  /stations/{station}:
    get:
      operationId: getStation
      description: "Get details of single station"
      tags:
        - stations
      parameters:
        - $ref: "#/components/parameters/Station"
      responses:
        200:
          description: "Station details"
          content:
            application/json:
              example: "./examples/stations/station-dto.json"
              schema:
                $ref: "./schemas/station.yml#/StationDto"
    put:
      operationId: updateStation
      description: "Update a single station"
      tags:
        - stations
      parameters:
        - $ref: "#/components/parameters/Station"
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/stations/update-station-request.json"
            schema:
              $ref: "./schemas/station.yml#/UpdateStationRequest"
      responses:
        204:
          description: "Station sensors successfully updated."

  /stations/{station}/sensors/{sensor}/settings:
    parameters:
      - $ref: "#/components/parameters/Station"
      - $ref: "#/components/parameters/Sensor"
    get:
      operationId: getSensorSettings
      description: "Retrieve the settings of a single sensor for the given station."
      tags:
        - sensor-settings
      responses:
        200:
          description: "The key-value map of settings that applies to this sensor. Empty when no settings were set."
          content:
            application/json:
              example: "./examples/stations/settings/get-sensor-settings-response.json"
              schema:
                $ref: "./schemas/station.yml#/SensorSettingsDto"
    put:
      operationId: setSensorSettings
      description: "Sets the settings of the sensor. Note: any previous settings are lost (not merged)!"
      tags:
        - sensor-settings
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/stations/settings/set-sensor-settings-request.json"
            schema:
              $ref: "./schemas/station.yml#/SensorSettingsDto"
      responses:
        204:
          description: "Settings applied successfully"

  /sensors:
    get:
      operationId: getSensors
      description: "Get a list of all known sensors and the metrics each sensor can measure."
      tags:
        - sensors
      responses:
        200:
          description: "List of sensors. Can be empty."
          content:
            application/json:
              example: "./examples/sensors/get-sensors-response.json"
              schema:
                $ref: "./schemas/sensor.yml#/GetSensorsResponse"
    post:
      operationId: postSensor
      description: "Add a new sensor that will be supported by the service."
      tags:
        - sensors
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/sensors/create-sensors-request.json"
            schema:
              $ref: "./schemas/sensor.yml#/CreateSensorDto"
      responses:
        204:
          description: "Sensor registered successfully"
        400:
          description: "Invalid sensor"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"

  /sensors/{id}:
    put:
      operationId: updateSensor
      description: "Update the properties of a sensor"
      tags:
        - sensors
      parameters:
        - $ref: "#/components/parameters/Id"
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/sensors/create-sensors-request.json"
            schema:
              $ref: "./schemas/sensor.yml#/UpdateSensorDto"
      responses:
        204:
          description: "Sensor updated successfully"

  /measurement-reports:
    post:
      operationId: postMeasurementReport
      description: "Process a set of observed measurements from a station for monitoring. Intermediate reports may be lost depending on the frequency and the timing of publishing to the monioting system."
      tags:
        - measurement reports
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/measurement-reports/post-measurement-report-request.json"
            schema:
              $ref: "./schemas/measurement-report.yml#/MeasurementReportDto"
      responses:
        204:
          description: "Measurement report successfully processed for monitoring."
        400:
          description: "The report was invalid. The station could be unknown or maybe a metric is not supported by the station."
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"

  /metrics:
    get:
      operationId: getMetrics
      description: "List all metrics"
      tags:
        - metrics
      responses:
        200:
          description: "A list of all the metrics."
          content:
            application/json:
              schema:
                $ref: "./schemas/metric.yml#/GetMetricsResponse"
    post:
      operationId: postMetric
      description: "Add a new metric that will be supported by the service."
      tags:
        - metrics
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/metrics/create-metrics-request.json"
            schema:
              $ref: "./schemas/metric.yml#/CreateMetricRequest"
      responses:
        200:
          description: "Metric registered successfully"
          content:
            application/json:
              schema:
                $ref: "./schemas/metric.yml#/MetricDto"
        400:
          description: "Invalid metric"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /metrics/{id}:
    patch:
      operationId: patchMetric
      description: "Update one or multiple properties of a metric. Be careful though, and make sure the metric is not used elsewhere."
      tags:
        - metrics
      parameters:
        - $ref: "#/components/parameters/Id"
      requestBody:
        required: true
        content:
          application/json:
            example: "./examples/metrics/create-metrics-request.json"
            schema:
              $ref: "./schemas/metric.yml#/UpdateMetricRequest"
      responses:
        204:
          description: "Metric updated successfully"
        400:
          description: "Invalid metric"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"

components:
  parameters:
    Id:
      name: id
      in: path
      required: true
      description: "The id of the resource to perform the operation on."
      schema:
        $ref: "./schemas/definitions.yml#/Id"
    Station:
      name: station
      in: path
      required: true
      description: "The name of the station to perform the operation on."
      schema:
        $ref: "./schemas/definitions.yml#/Name"
    Sensor:
      name: sensor
      in: path
      required: true
      description: "The name of the sensor to perform the operation on."
      schema:
        $ref: "./schemas/definitions.yml#/Name"
  schemas:
    Error:
      type: object
      properties:
        message:
          type: string
          description: "Message describing why the request failed."
          example: "Sensor not found"
