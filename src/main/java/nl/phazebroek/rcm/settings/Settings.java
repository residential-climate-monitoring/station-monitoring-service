/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.station.Station;

/**
 * Settings for a specific sensor at a specific station. Settings can be any key-value pair and are all stored as string.
 * Sensor settings can be retrieved by the station-client and apply them before reading from the sensor.
 *
 * @since 3.0.0
 */
@Entity
@Getter
@Setter
public class Settings {

    @Id
    @GeneratedValue(generator = "system-uuid")
    private UUID id;

    @ManyToOne(fetch = FetchType.EAGER)
    private Station station;

    @ManyToOne(fetch = FetchType.EAGER)
    private Sensor sensor;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> settings = new HashMap<>();

}
