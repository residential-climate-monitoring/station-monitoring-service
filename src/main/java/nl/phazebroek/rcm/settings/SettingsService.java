/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import static java.util.Collections.emptyMap;

import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.model.SensorSettingsDto;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.station.Station;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service that handles sensor settings.
 *
 * @since 3.0.0
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SettingsService {

    private final SettingsRepository settingsRepository;

    private final SettingsMapper settingsMapper;

    public Optional<Settings> findSettingsByStationAndSensor(Station station, Sensor sensor) {
        log.debug("Look up sensor settings for station [{}] and sensor [{}]", station.getName(), sensor.getName());
        return settingsRepository.findByStationAndSensor(station, sensor);
    }

    /**
     * Return the sensor settings for the given sensor and station. The sensor should be part of the station.
     * For backwards compatibility, if no sensor settings are found, fresh new empty sensor settings will be initialized. Doing so will
     * avoid the station-client receiving a 404 not found.
     */
    public SensorSettingsDto getSensorSettingsDto(Station station, Sensor sensor) {
        log.debug("Get sensor settings for station [{}] and sensor [{}]", station.getName(), sensor.getName());
        var settings = findSettingsByStationAndSensor(station, sensor)
            .orElseGet(() -> createSettings(station, sensor, null));
        return settingsMapper.toSensorSettingsDto(settings.getSettings());
    }

    /**
     * Create and persist sensor settings.
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public Settings createSettings(Station station, Sensor sensor, Map<String, Object> settings) {
        log.info("Creating sensor settings for sensor [{}] at station [{}]", sensor.getName(), station.getName());
        var sensorSettings = settingsMapper.toSensorSettings(station, sensor, settings);
        return settingsRepository.save(sensorSettings);
    }

    /**
     * Update the settings for a specific sensor on a specific station. Any previous settings are lost.
     * Does not fail if no sensor settings exists yet.
     */
    @Transactional
    public void updateSettings(Station station, Sensor sensor, Map<String, Object> settings) {
        log.info("Saving settings for station [{}] and sensor [{}]", station.getName(), sensor.getName());
        var sensorSettings = settingsRepository
            .findByStationAndSensor(station, sensor)
            .orElseGet(() -> settingsMapper.toSensorSettings(station, sensor, emptyMap()));

        var mappedSettings = settingsMapper.toMapWithStrings(settings);
        sensorSettings.setSettings(mappedSettings);

        settingsRepository.save(sensorSettings);
        log.info("Sensor settings saved");
    }

}
