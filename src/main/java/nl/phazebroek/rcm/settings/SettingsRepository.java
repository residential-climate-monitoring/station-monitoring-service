/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import java.util.Optional;
import java.util.UUID;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.station.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository for sensor settings.
 *
 * @since 3.0.0
 */
@Repository
public interface SettingsRepository extends JpaRepository<Settings, UUID> {

    Optional<Settings> findByStationAndSensor(Station station, Sensor sensor);

}
