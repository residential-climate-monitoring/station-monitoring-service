/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.settings;

import java.util.HashMap;
import java.util.Map;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.SensorSettingsDto;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.station.Station;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.springframework.util.CollectionUtils;

/**
 * Mapper for sensor settings.
 *
 * @since 3.0.0
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
interface SettingsMapper {

    @Mapping(target = "settings", source = "source", qualifiedByName = "toMapWithObjects")
    SensorSettingsDto toSensorSettingsDto(Map<String, String> source);

    @Mapping(target = "id", ignore = true)
    Settings toSensorSettings(Station station, Sensor sensor, Map<String, Object> settings);

    @Named("toMapWithObjects")
    Map<String, Object> toMapWithObjects(Map<String, String> source);

    /**
     * Takes care of the conversion from {@code Map<String, Object>} to {@code Map<String, String>}.
     */
    default Map<String, String> toMapWithStrings(Map<String, Object> source) {
        if (CollectionUtils.isEmpty(source)) {
            return new HashMap<>();
        }
        var target = new HashMap<String, String>(source.size());

        source.entrySet().stream()
            .filter(entry -> entry.getValue() instanceof String)
            .forEach(entry -> target.put(entry.getKey(), (String) entry.getValue()));

        if (target.size() != source.size()) {
            throw new BadRequestException("All values must be strings.");
        }
        return target;
    }

}
