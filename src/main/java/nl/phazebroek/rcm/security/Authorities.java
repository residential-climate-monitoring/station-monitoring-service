/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.security;

/**
 * Simple utility class with the String constants of the various authorities used throughout the app.
 */
public abstract class Authorities {

    public static final String PREFIX = "SCOPE_";
    public static final String SUBMIT_REPORT = PREFIX + "submit:report";

    private Authorities() {
    }

}
