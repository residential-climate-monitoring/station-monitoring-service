/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.security;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.util.CollectionUtils;

@Slf4j
@Configuration
@EnableWebSecurity
@ConditionalOnMissingBean(NoSecurityConfiguration.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * The namespace of custom claims.
     */
    private static final String CLAIM_NAMESPACE = "http://rcm:eu:auth0:com";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .mvcMatchers("/", "/favicon.ico", "/actuator/prometheus").permitAll()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling(e -> e.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)))
            .csrf(c -> c
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
            )
            .logout(l -> l.logoutSuccessUrl("/bye").permitAll())
            .oauth2Login()
            .and()
            .oauth2ResourceServer().jwt();
    }

    /**
     * Intercepts the authentication process and enrich the OAuth2 (with Open ID Connect) user with custom granted authorities based on the
     * roles that are found in the claim "rcm.eu.auth0.com/roles".
     *
     * @implNote the claim is populated through a custom rule in auth0.
     */
    @Bean
    public OAuth2UserService<OidcUserRequest, OidcUser> userService() {
        var openIdConnectUserService = new OidcUserService();

        return request -> {
            var user = openIdConnectUserService.loadUser(request);
            List<String> roles = user.getUserInfo().getClaim(String.format("%s/roles", CLAIM_NAMESPACE));
            log.info("Detected custom roles in claim: [{}]", roles);
            var idToken = request.getIdToken();
            var mappedAuthorities = toGrantedAuthorities(roles, idToken, user);
            return new DefaultOidcUser(mappedAuthorities, idToken, user.getUserInfo());
        };
    }

    /**
     * Maps a set of roles, the idToken and user info to a set of granted authorities.
     */
    private Set<GrantedAuthority> toGrantedAuthorities(List<String> roles, OidcIdToken token, OidcUser user) {
        if (CollectionUtils.isEmpty(roles)) {
            roles = List.of("guest");
        }
        Set<GrantedAuthority> mappedAuthorities = roles
            .stream()
            .map(role -> new OidcUserAuthority(Roles.PREFIX + role.toUpperCase(), token, user.getUserInfo()))
            .collect(toSet());
        mappedAuthorities.addAll(user.getAuthorities());
        return mappedAuthorities;
    }
}
