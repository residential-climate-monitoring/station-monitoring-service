/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.security;

/**
 * Simple utility class with the String constants of the various roles used throughout the app.
 */
public abstract class Roles {

    public static final String PREFIX = "ROLE_";
    public static final String GUEST = PREFIX + "GUEST";
    public static final String USER = PREFIX + "USER";
    public static final String REPORTER = PREFIX + "REPORTER";
    public static final String ADMIN = PREFIX + "ADMIN";

    private Roles() {
    }

}
