/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue.model;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.Nullable;

/**
 * Base class for Philips Hue Sensors as exposed by the adapter to hide the internal Hue API semantics.
 */
@Getter
@Setter
@ToString
public abstract class HueSensor {

    private String name;

    private boolean on;

    @Nullable
    private LocalDateTime lastUpdated;

}

