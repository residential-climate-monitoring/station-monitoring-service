/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * A Philips Hue Motion Sensor actually consists of three sensors: presence, temperature and light level. This class represents a single
 * physical motion sensor unit with all metrics of its individual hardware sensors combined.
 */
@Getter
@Setter
@ToString
public class HueMotionSensor extends HueSensor {

    private double battery;

    private boolean reachable;

    private boolean presence;

    private boolean dark;

    private boolean daylight;

    private double lightLevel;

    private double temperature;

    private String modelId;

}

