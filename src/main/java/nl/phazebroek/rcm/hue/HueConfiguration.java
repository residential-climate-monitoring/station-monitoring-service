/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration for the client of the Philips Hue Bridge API.
 */
@Configuration
@ConditionalOnProperty("rcm.hue.bridge.base-url")
@RequiredArgsConstructor
public class HueConfiguration {

    private final WebClient.Builder webClientBuilder;

    @Getter
    @Value("${rcm.hue.motion-sensor.default-name:Hue Motion Sensor}")
    public String motionSensorName;

    @Value("${rcm.hue.bridge.base-url}")
    private String hueBridgeBaseUrl;

    /**
     * Initializes a web client pre-configured with the base-url of the Philips Hue Bridge.
     */
    @Bean
    protected WebClient hueBridgeWebClient() {
        return webClientBuilder
            .baseUrl(hueBridgeBaseUrl)
            .build();
    }

    @Bean
    public HueClient hueBridgeClient() {
        return new HueClient(hueBridgeWebClient());
    }
}
