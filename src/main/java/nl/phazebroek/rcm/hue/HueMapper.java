/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;
import nl.phazebroek.rcm.hue.model.HueMotionSensor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;

/**
 * Maps the elements returned from the Philips Hue Bridge API.
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface HueMapper {

    /**
     * Combines the presence, light level and temperature sensors together into a single motion sensor. For the common properties, the data
     * of the presence sensor is used.
     */
    @Mapping(target = "name", source = "presenceSensor.name")
    @Mapping(target = "on", source = "presenceSensor.config.on")
    @Mapping(target = "lastUpdated", source = "presenceSensor.state.lastUpdated", qualifiedByName = "mapDateTime")
    @Mapping(target = "battery", source = "presenceSensor.config.battery", qualifiedByName = "divideByOneHundred")
    @Mapping(target = "reachable", source = "presenceSensor.config.reachable")
    @Mapping(target = "presence", source = "presenceSensor.state.presence")
    @Mapping(target = "modelId", source = "presenceSensor.modelId")
    @Mapping(target = "dark", source = "lightLevelSensor.state.dark")
    @Mapping(target = "daylight", source = "lightLevelSensor.state.daylight")
    @Mapping(target = "lightLevel", source = "lightLevelSensor.state.lightLevel")
    @Mapping(target = "temperature", source = "temperatureSensor.state.temperature", qualifiedByName = "divideByOneHundred")
    HueMotionSensor toMotionSensor(HuePresenceSensor presenceSensor, HueLightLevelSensor lightLevelSensor,
        HueTemperatureSensor temperatureSensor);

    /**
     * Divides the value by 100 and convert it to a double. This is used to map values like temperature and battery percentage.
     * E.g. 2134 means 21.34 °C and 98 means 98 % (i.e. 0.98).
     */
    @Named("divideByOneHundred")
    default double divideByOneHundred(int value) {
        return ((double) value) / 100;
    }

    /**
     * Sometimes the Hue Bridge API returns "none" as value for lastUpdated. This mapping will safely return a local date time instance or
     * null if there was no date time present.
     */
    @Named("mapDateTime")
    default LocalDateTime mapDateTime(String source) {
        try {
            return LocalDateTime.parse(source);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}
