/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Mapping of the "modelId" values returned by the Hue Sensors API.
 */
@NoArgsConstructor(access = AccessLevel.NONE)
public abstract class ModelId {

    /**
     * Hue Motion Sensor.
     */
    public static final String SML001 = "SML001";

    /**
     * Hue Outdoor Sensor.
     */
    public static final String SML002 = "SML002";

    public static final List<String> INDOOR = List.of(SML001);

    public static final List<String> OUTDOOR = List.of(SML002);

}
