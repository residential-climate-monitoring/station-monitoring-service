/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.hue.api.HueSensorDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Client that calls the Hue Bridge API.
 */
@Slf4j
@RequiredArgsConstructor
public class HueClient {

    private final WebClient hueBridgeWebClient;

    /**
     * Get all the sensors. The API returns the sensors as a key-value map where the ID of the sensor is the key. This methods just returns
     * a list of sensors because we don't care about the ID's. A hue motion sensor for instance, is made up of three individual sensors,
     * we don't want to keep a list of each individual sensor id.
     */
    public Mono<List<HueSensorDto>> getSensors() {
        log.debug("Looking for Philips Hue Sensors");
        return hueBridgeWebClient
            .get()
            .uri("/sensors")
            .retrieve()
            .bodyToMono(new ParameterizedTypeReference<Map<String, HueSensorDto>>() {})
            .map(this::toList)
            .doOnSuccess(response -> log.debug("Found [{}] sensors", response.size()))
            .doOnError(this::logError);
    }

    private List<HueSensorDto> toList(Map<String, HueSensorDto> hueSensorsById) {
        return new ArrayList<>(hueSensorsById.values());
    }

    private void logError(Throwable throwable) {
        log.error("An error occurred while communicating with the Philips Hue Bridge: [{}]", throwable.getMessage());
    }

}
