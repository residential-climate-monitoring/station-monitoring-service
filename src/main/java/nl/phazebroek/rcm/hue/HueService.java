/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static java.time.ZoneOffset.UTC;
import static nl.phazebroek.rcm.metric.DefaultMetric.BATTERY;
import static nl.phazebroek.rcm.metric.DefaultMetric.LUX;
import static nl.phazebroek.rcm.metric.DefaultMetric.PRESENCE;
import static nl.phazebroek.rcm.metric.DefaultMetric.TEMPERATURE;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.hue.api.HueSensorDto;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;
import nl.phazebroek.rcm.hue.model.HueMotionSensor;
import nl.phazebroek.rcm.measurement.MeasurementService;
import nl.phazebroek.rcm.station.StationService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * This service integrates with the hue bridge, polls it regularly for data and generates measurement reports.
 */
@Slf4j
@Service
@ConditionalOnBean(HueClient.class)
@RequiredArgsConstructor
public class HueService {

    private static final int SCHEDULE_INTERVAL_MS = 60_000;

    /**
     * The value in the measurement report when motion is detected.
     */
    private static final double MOTION_DETECTED = 1.0;

    /**
     * The value in the measurement report when *NO* motion is detected.
     */
    private static final double NO_MOTION_DETECTED = 0.0;

    private final HueClient hueClient;

    private final HueMapper hueMapper;

    private final HueConfiguration hueConfiguration;

    private final StationService stationService;

    private final MeasurementService measurementService;

    /**
     * Contact the Philips Hue Bridge on a frequent basis, gather the details from its sensors and report their status.
     */
    @Scheduled(fixedRate = SCHEDULE_INTERVAL_MS)
    public void monitorSensors() {
        hueClient.getSensors()
            .map(this::toMotionSensors)
            .doOnNext(this::autoCreateStations)
            .subscribe(
                this::reportMotionSensorStatus,
                error -> log.error("An error occurred trying to monitor Philips Hue Sensors: [{}]", error.getMessage()));
    }

    //<editor-fold desc="Hue API specific, good candidate to extract to a separate library in the future">
    /**
     * Merge the individual physical hardware sensors to one logical motion sensors. The hardware sensors of the same motion sensor share
     * the same uniqueId for the first 22 characters. This common uniqueId is used to group them together and merge them into a single
     * motion sensor instance.
     */
    private List<HueMotionSensor> toMotionSensors(List<HueSensorDto> hueSensorDtos) {
        var sensorsByUniqueId = new HashMap<String, List<HueSensorDto>>();

        hueSensorDtos.stream()
            .filter(this::instanceOfMotionSensorTypes)
            .forEach(hueSensor -> {
                var commonUniqueId = hueSensor.getUniqueId().substring(0, 23);
                sensorsByUniqueId
                    .computeIfAbsent(commonUniqueId, key -> new ArrayList<>())
                    .add(hueSensor);
            });

        return sensorsByUniqueId.values().stream()
            .map(this::toMotionSensor)
            .toList();
    }

    private boolean instanceOfMotionSensorTypes(HueSensorDto sensor) {
        return sensor instanceof HuePresenceSensor || sensor instanceof HueLightLevelSensor || sensor instanceof HueTemperatureSensor;
    }

    /**
     * Merge a list of physical sensors into a single logical motion sensor.
     *
     * @param hueSensors the physical hue sensors, they should be from the same logical motion sensor.
     */
    private HueMotionSensor toMotionSensor(List<HueSensorDto> hueSensors) {
        var presenceSensor = getSensorOfType(hueSensors, HuePresenceSensor.class);
        var lightLevelSensor = getSensorOfType(hueSensors, HueLightLevelSensor.class);
        var temperatureSensor = getSensorOfType(hueSensors, HueTemperatureSensor.class);
        correctPresenceForInterval(presenceSensor);
        return hueMapper.toMotionSensor(presenceSensor, lightLevelSensor, temperatureSensor);
    }

    /**
     * The state of a Philips Hue Presence sensor is reset after about 10 seconds. Meaning that if monitoring interval is less than 10
     * seconds it could fail to detect motion. This method takes the last updated property of the sensor into account and resets the
     * presence flag to true if the time since last update was less than the monitoring interval.
     */
    private void correctPresenceForInterval(HuePresenceSensor presenceSensor) {
        LocalDateTime lastUpdated;

        try {
            lastUpdated = LocalDateTime.parse(presenceSensor.getState().getLastUpdated());
        } catch (DateTimeParseException e) {
            log.warn("Failed to correct presence of [{}] due to missing lastUpdated time.", presenceSensor.getName());
            return;
        }

        var now = LocalDateTime.now(UTC);
        if (Duration.between(lastUpdated, now).toMillis() < SCHEDULE_INTERVAL_MS) {
            log.debug("Presence of [{}] overridden because time since last update within schedule interval", presenceSensor.getName());
            presenceSensor.getState().setPresence(true);
        }
    }

    /**
     * Looks for a sensor in a list of sensor of a given type and returns it cast to that type.
     *
     * @param sensors A list of sensors, each a subtype of {@link HueSensorDto}.
     * @param type    the class definition of the type of sensor to find in the list of sensors.
     * @param <T>     the actual type of sensor
     * @return the first sensor of the given type that was present in the list of sensors, or null when not present.
     */
    private <T extends HueSensorDto> T getSensorOfType(List<? extends HueSensorDto> sensors, Class<T> type) {
        return sensors.stream()
            .filter(type::isInstance)
            .findFirst()
            .map(type::cast)
            .orElseThrow(() -> new IllegalArgumentException(String.format("Missing sensor of type [%s]", type.getSimpleName())));
    }
    //</editor-fold>

    //<editor-fold desc="This logic is core RCM stuff and depends too much on its internals that it cannot be extracted to a library.">
    private void autoCreateStations(List<HueMotionSensor> hueMotionSensors) {
        hueMotionSensors.forEach(this::autoCreateStation);
    }

    private void autoCreateStation(HueMotionSensor hueMotionSensor) {
        if (stationService.findStationByName(hueMotionSensor.getName()).isEmpty()) {
            log.info("Auto-creating station for [{}]", hueMotionSensor.getName());
            var request = new CreateStationRequest()
                .name(hueMotionSensor.getName())
                .indoor(ModelId.INDOOR.contains(hueMotionSensor.getModelId()))
                .sensors(List.of(new SensorWithSettingsDto().name(hueConfiguration.getMotionSensorName())));
            stationService.addStation(request);
        }
    }

    /**
     * Generate a measurement report for each hue motion sensor and process that report for monitoring.
     */
    private void reportMotionSensorStatus(List<HueMotionSensor> motionSensors) {
        log.debug("Reporting status of [{}] motion sensors", motionSensors.size());

        motionSensors.stream()
            .map(this::toMeasurementReport)
            .forEach(measurementService::processMeasurementReport);
    }

    private MeasurementReportDto toMeasurementReport(HueMotionSensor motionSensor) {
        return new MeasurementReportDto()
            .stationName(motionSensor.getName())
            .measurements(Map.of(
                PRESENCE.getName(), motionSensor.isPresence() ? MOTION_DETECTED : NO_MOTION_DETECTED,
                LUX.getName(), motionSensor.getLightLevel(),
                TEMPERATURE.getName(), motionSensor.getTemperature(),
                BATTERY.getName(), motionSensor.getBattery()
            ));
    }
    //</editor-fold>

}
