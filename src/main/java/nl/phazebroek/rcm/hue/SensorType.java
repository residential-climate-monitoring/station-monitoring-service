/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Mapping of the "type" values returned by the Hue Sensors API.
 */
@NoArgsConstructor(access = AccessLevel.NONE)
public abstract class SensorType {

    public static final String PRESENCE = "ZLLPresence";

    public static final String LIGHT = "ZLLLightLevel";

    public static final String TEMPERATURE = "ZLLTemperature";

    public static final String DAYLIGHT = "Daylight";

    public static final String GEOFENCE = "Geofence";

    public static final List<String> MOTION = List.of(PRESENCE, LIGHT, TEMPERATURE);

}
