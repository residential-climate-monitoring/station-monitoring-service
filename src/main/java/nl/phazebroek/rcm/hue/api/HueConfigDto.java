/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

/**
 * The configuration of a sensor as returned by the Philips Hue Bridge.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HueConfigDto {

    private boolean on;

    private int battery;

    private boolean reachable;

}
