/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;
import nl.phazebroek.rcm.hue.SensorType;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueDaylightSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueGeofenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueLightLevelSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HuePresenceSensor;
import nl.phazebroek.rcm.hue.api.HueSensorDto.HueTemperatureSensor;

/**
 * The root element returned by the /api/sensors resource of the Hue Bridge API. The sub types are used to make it easier to handle each
 * different type of sensor individually.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    property = "type",
    defaultImpl = HueSensorDto.class
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = HueDaylightSensor.class, name = SensorType.DAYLIGHT),
    @JsonSubTypes.Type(value = HueLightLevelSensor.class, name = SensorType.LIGHT),
    @JsonSubTypes.Type(value = HuePresenceSensor.class, name = SensorType.PRESENCE),
    @JsonSubTypes.Type(value = HueTemperatureSensor.class, name = SensorType.TEMPERATURE),
    @JsonSubTypes.Type(value = HueGeofenceSensor.class, name = SensorType.GEOFENCE)
})
public class HueSensorDto {

    private String name;

    private String type;

    @JsonProperty("modelid")
    private String modelId;

    @JsonProperty("uniqueid")
    private String uniqueId;

    private HueStateDto state;

    private HueConfigDto config;

    @JsonTypeName(SensorType.DAYLIGHT)
    public static class HueDaylightSensor extends HueSensorDto {
    }

    @JsonTypeName(SensorType.LIGHT)
    public static class HueLightLevelSensor extends HueSensorDto {
    }

    @JsonTypeName(SensorType.PRESENCE)
    public static class HuePresenceSensor extends HueSensorDto {
    }

    @JsonTypeName(SensorType.TEMPERATURE)
    public static class HueTemperatureSensor extends HueSensorDto {
    }

    @JsonTypeName(SensorType.GEOFENCE)
    public static class HueGeofenceSensor extends HueSensorDto {
    }

}
