/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * The state of a sensor as returned by the Philips Hue Bridge. This is the generic state Dto that holds the state for various types of
 * sensors. Depending on the type, only some properties will be present per instance.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HueStateDto {

    private boolean presence;

    private boolean dark;

    private boolean daylight;

    @JsonProperty("lightlevel")
    private int lightLevel;

    private int temperature;

    @JsonProperty("lastupdated")
    private String lastUpdated;

}
