/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.hue;

import static nl.phazebroek.rcm.metric.DefaultMetric.BATTERY;
import static nl.phazebroek.rcm.metric.DefaultMetric.LUX;
import static nl.phazebroek.rcm.metric.DefaultMetric.PRESENCE;
import static nl.phazebroek.rcm.metric.DefaultMetric.TEMPERATURE;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.sensor.SensorService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Creates the hue motion sensor if it doesn't exist yet.
 */
@Slf4j
@Component
@Order(20)
@RequiredArgsConstructor
@ConditionalOnProperty(value = "rcm.sensors.auto-create-enabled", havingValue = "true", matchIfMissing = true)
@ConditionalOnBean(HueConfiguration.class)
public class HueSensorInitializer implements ApplicationRunner {

    private final SensorService sensorService;

    private final HueConfiguration hueConfiguration;

    @Override
    public void run(ApplicationArguments args) {
        var motionSensorName = hueConfiguration.getMotionSensorName();
        if (sensorService.sensorExistsWithName(motionSensorName)) {
            return;
        }
        log.info("Creating default sensor [{}]", motionSensorName);
        var createSensorDto = new CreateSensorDto()
            .name(motionSensorName)
            .metrics(List.of(PRESENCE.getName(), TEMPERATURE.getName(), LUX.getName(), BATTERY.getName()));
        sensorService.addSensor(createSensorDto);
    }
}
