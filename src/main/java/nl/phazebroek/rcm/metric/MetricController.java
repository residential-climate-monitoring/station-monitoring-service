/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static nl.phazebroek.rcm.security.Roles.ADMIN;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import nl.phazebroek.rcm.api.MetricsApi;
import nl.phazebroek.rcm.api.model.CreateMetricRequest;
import nl.phazebroek.rcm.api.model.GetMetricsResponse;
import nl.phazebroek.rcm.api.model.MetricDto;
import nl.phazebroek.rcm.api.model.UpdateMetricRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MetricController implements MetricsApi {

    private final MetricService metricService;

    @Override
    public ResponseEntity<GetMetricsResponse> getMetrics() {
        var metrics = metricService.getMetrics();
        return ResponseEntity.ok(new GetMetricsResponse().metrics(metrics));
    }

    @Override
    @Secured(ADMIN)
    public ResponseEntity<MetricDto> postMetric(CreateMetricRequest request) {
        var metric = metricService.createMetric(request);
        return ResponseEntity.ok(metric);
    }

    @Override
    @Secured(ADMIN)
    public ResponseEntity<Void> patchMetric(UUID id, UpdateMetricRequest request) {
        metricService.updateMetric(id, request);
        return ResponseEntity.noContent().build();
    }
}
