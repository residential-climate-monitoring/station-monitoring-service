/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import lombok.Getter;

/**
 * A set of default metrics.
 */
@Getter
public enum DefaultMetric {

    // Default hardware sensors:
    TEMPERATURE("temperature", Unit.CELSIUS),
    HUMIDITY("humidity", Unit.PERCENT),
    AIR_PRESSURE("air-pressure", "hPa"),
    VOC_GAS("voc-gas", "Ω"),
    LUX("lux", "lx"),
    VISIBLE_LIGHT("visible-light", Unit.NONE),
    IR_LIGHT("IR-light", Unit.NONE),
    ECO_2("eCO2", "ppm"),
    TVOC("TVOC", "ppb"),
    RAW_H2("raw-H2", Unit.NONE),
    RAW_ETHANOL("raw-ethanol", Unit.NONE),
    BATTERY("battery", Unit.PERCENT),
    PRESENCE("presence", Unit.NONE),

    // Metrics from psutils library:
    DISK_TOTAL("disk-total", Unit.BYTES),
    DISK_FREE("disk-free", Unit.BYTES),
    DISK_USED("disk-used", Unit.BYTES),

    MEM_TOTAL("mem-total", Unit.BYTES),
    MEM_AVAILABLE("mem-available", Unit.BYTES),
    MEM_USED("mem-used", Unit.BYTES),

    LOAD_AVG_1("load-avg-1", Unit.NONE),
    LOAD_AVG_5("load-avg-5", Unit.NONE),
    LOAD_AVG_15("load-avg-15", Unit.NONE),

    NET_BYTES_SENT("net-bytes-sent", Unit.BYTES),
    NET_BYTES_RECV("net-bytes-recv", Unit.BYTES),
    NET_PACKETS_SENT("net-packets-sent", Unit.NONE),
    NET_PACKETS_RECV("net-packets-recv", Unit.NONE),
    NET_ERRIN("net-errin", Unit.NONE),
    NET_ERROUT("net-errout", Unit.NONE),
    NET_DROPIN("net-dropin", Unit.NONE),
    NET_DROPOUT("net-dropout", Unit.NONE),

    CPU_COUNT("cpu-count", Unit.NONE),
    CPU_FREQ("cpu-frequency", "MHz"),
    CPU_TEMP("cpu-temp", Unit.CELSIUS),
    CPU_PERCENT("cpu-percent", Unit.PERCENT),

    SYS_BOOT_TIME("sys-boot-time", Unit.NONE);

    private final String name;

    private final String unit;

    DefaultMetric(String name, String unit) {
        this.name = name;
        this.unit = unit;
    }

    private static class Unit {

        public static final String BYTES = "bytes";
        public static final String NONE = "";
        public static final String CELSIUS = "°C";
        public static final String PERCENT = "%";
    }
}
