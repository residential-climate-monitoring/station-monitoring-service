/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.CreateMetricRequest;
import nl.phazebroek.rcm.api.model.UpdateMetricRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@Component
@RequiredArgsConstructor
public class MetricValidator {

    private final MetricRepository metricRepository;

    /**
     * Validate if the metrics in the list exists.
     *
     * @param metrics the list of metrics to validate.
     * @throws BadRequestException if any of the metrics is not supported.
     */
    public void validate(List<String> metrics) {
        log.debug("Validating metrics [{}] exists", metrics);
        var validMetrics = metricRepository.findAll()
            .stream().map(Metric::getName)
            .collect(Collectors.toSet());

        var invalidMetrics = metrics.stream()
            .filter(metric -> !validMetrics.contains(metric))
            .sorted()
            .collect(Collectors.joining(", "));

        if (StringUtils.hasText(invalidMetrics)) {
            throw new ResponseStatusException(BAD_REQUEST, String.format("Invalid metric(s): %s", invalidMetrics));
        }
    }

    /**
     * Validates the request to create a new metric.
     */
    public void validate(CreateMetricRequest request) {
        validateUniqueness(request.getName(), request.getUnit());
    }

    /**
     * Validates the request to update a metric contains at least one property.
     */
    public void validate(UpdateMetricRequest request) {
        if (request.getName() == null && request.getUnit() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Request must have at least a name or unit");
        }
    }

    /**
     * Validates the name and unit of the metric are unique.
     */
    public void validateUniqueness(String name, String unit) {
        log.debug("Validating that no metric with name [{}] and unit [{}] exists yet", name, unit);
        if (metricRepository.existsByNameAndUnit(name, unit)) {
            throw new ResponseStatusException(BAD_REQUEST, "Metric must have unique name and unit");
        }
    }
}
