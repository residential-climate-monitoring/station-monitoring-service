/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * A metric defines a single physical quantity. Optionally the unit is also defined. It is perfectly fine to define the same metric
 * more than once but each with a different unit. An example is VOC gas which is measured without unit by the BME680 sensor whereas the
 * SGP30 sensor measures it in ppm. Note however that the name is unique and used as identifier in some cases for ease of use.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Metric {

    @Id
    @GeneratedValue(generator = "system-uuid")
    private UUID id;

    @Column(length = 50)
    private String name;

    @Column(length = 50)
    private String unit;

    public Metric(String name, String unit) {
        this.name = name;
        this.unit = unit;
    }
}
