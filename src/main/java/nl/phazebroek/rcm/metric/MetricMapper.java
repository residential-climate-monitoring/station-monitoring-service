/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import java.util.List;
import nl.phazebroek.rcm.api.model.CreateMetricRequest;
import nl.phazebroek.rcm.api.model.MetricDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
interface MetricMapper {

    MetricDto toMetricDto(Metric source);

    List<MetricDto> toMetricDto(List<Metric> source);

    @Mapping(target = "id", ignore = true)
    Metric toMetric(CreateMetricRequest source);

}
