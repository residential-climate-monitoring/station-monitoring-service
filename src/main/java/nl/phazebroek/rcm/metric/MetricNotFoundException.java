/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MetricNotFoundException extends RuntimeException {

    public MetricNotFoundException(UUID id) {
        super(String.format("Metric with ID [%s] not found", id));
    }
}
