/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.model.CreateMetricRequest;
import nl.phazebroek.rcm.api.model.MetricDto;
import nl.phazebroek.rcm.api.model.UpdateMetricRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class MetricService {

    private final MetricRepository metricRepository;

    private final MetricMapper metricMapper;

    private final MetricValidator metricValidator;

    public Metric getMetric(UUID id) {
        return metricRepository.findById(id).orElseThrow(() -> new MetricNotFoundException(id));
    }

    /**
     * Get a list of all the metrics.
     */
    public List<MetricDto> getMetrics() {
        var metrics = metricRepository.findAll(Sort.by(Direction.ASC, "name"));
        return metricMapper.toMetricDto(metrics);
    }

    /**
     * Create a new metric.
     *
     * @return the dto of the metric to return by the API.
     */
    public MetricDto createMetric(CreateMetricRequest request) {
        log.debug("Creating metric [{}]", request);

        metricValidator.validate(request);

        var metric = metricMapper.toMetric(request);

        metric = metricRepository.save(metric);

        return metricMapper.toMetricDto(metric);
    }

    /**
     * Perform updates to the metric with the given id.
     *
     * @param id      the id of the metric to update.
     * @param request the updates to apply to the metric.
     */
    public void updateMetric(UUID id, UpdateMetricRequest request) {
        log.debug("Updating metric [{}] with [{}]", id, request);

        metricValidator.validate(request);

        var metric = getMetric(id);

        if (StringUtils.hasText(request.getName())) {
            metric.setName(request.getName());
        }
        if (StringUtils.hasText(request.getUnit())) {
            metric.setUnit(request.getUnit());
        }

        metricValidator.validateUniqueness(metric.getName(), metric.getUnit());
        metricRepository.save(metric);
    }
}
