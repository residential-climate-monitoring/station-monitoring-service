/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.metric;

import java.util.Arrays;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Automatically creates default metrics on application start-up when not present.
 */
@Slf4j
@Component
@Order(10)
@ConditionalOnProperty(value = "rcm.metrics.auto-create-enabled", havingValue = "true", matchIfMissing = true)
@RequiredArgsConstructor
public class MetricInitializer implements ApplicationRunner {

    private final MetricRepository metricRepository;

    @Override
    public void run(ApplicationArguments args) {
        var metricNames = Arrays.stream(DefaultMetric.values())
            .map(DefaultMetric::getName)
            .toList();

        var existingMetrics = metricRepository.findAllByNameIn(metricNames).stream().map(Metric::getName).toList();

        var newMetrics = Arrays.stream(DefaultMetric.values())
            .filter(defaultMetric -> !existingMetrics.contains(defaultMetric.getName()))
            .map(defaultMetric -> new Metric(defaultMetric.getName(), defaultMetric.getUnit()))
            .toList();

        log.info("Creating default metrics: [{}]", newMetrics);

        metricRepository.saveAll(newMetrics);
    }
}
