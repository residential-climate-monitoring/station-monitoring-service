/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.monitoring;

import com.google.common.util.concurrent.AtomicDouble;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.measurement.Measurement;
import nl.phazebroek.rcm.station.Station;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Service that handles the monitoring of sensors.
 * It uses two meters:
 * <pre>
 * - `sensor.readings` this contains the values measured by the sensors. It contains tags that describe the metric and station.
 * - `rcm.measurement.counter` is a counter for the number of measurements that are collected.
 * </pre>
 * The sensor.readings contains gauges whose value can go up and down. Gauges need to be updated frequently in order to keep them 'alive'.
 * If a gauge was not updated for some time it will be removed.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MonitoringService {

    private static final String SENSOR_GAUGE_NAME = "sensor.readings";

    private static final String MEASUREMENT_COUNTER = "rcm.measurement.counter";

    private final MeterRegistry registry;

    private final Map<String, AtomicDouble> gauges = new ConcurrentHashMap<>();

    private final Map<String, Counter> counters = new ConcurrentHashMap<>();

    private final Map<String, Long> gaugesUpdatedAt = new ConcurrentHashMap<>();

    @Value("${rcm.monitoring.max-idle-time-ms}")
    private int maxIdleTime;

    /**
     * Updates the gauge for the given measurement. Note, updating the gauge very frequently is useless as only the
     * current value of the gauge is monitored at the time of observation (when the data of the meter registry is
     * published).
     */
    public void updateGauge(Measurement measurement) {
        var metric = measurement.getMetric();
        var stationName = measurement.getStation().getName();
        var value = measurement.getValue();
        log.debug("Updating [{}] gauge for station [{}] to: [{}{}]", metric.getName(), stationName, value, metric.getUnit());
        var gauge = getGauge(measurement.getStation(), metric.getName());
        gauge.set(value);
    }

    /**
     * Retrieve the value observed by the gauge which is used to monitor the given metric for the given station. When
     * none exists a new one is created.
     *
     * @param station    the station to retrieve the gauge for.
     * @param metricName the metric being monitoring.
     * @return the AtomicDouble which represents the value of the gauge. This value should be updated to update the
     *     gauge.
     * @implNote this method it does not return a reference to the gauge itself because of the "heigen-gauge"
     *     principle. Gauges are self sufficient once created. A gauge cannot be created with primitives numbers
     *     since they are immutable. Re-creating the gauge with the new value won't work as the registry only
     *     maintains one meter for each unique combination of name and tags.
     */
    private AtomicDouble getGauge(Station station, String metricName) {
        var key = String.format("%s.%s", station.getName(), metricName);
        gaugesUpdatedAt.put(key, System.currentTimeMillis());
        return gauges.computeIfAbsent(key, s -> createGauge(station, metricName));
    }

    /**
     * Create a gauge for the given station and metric. Bu default the gauge has a value of zero.
     *
     * @return the value observed by the gauge.
     */
    private AtomicDouble createGauge(Station station, String metricName) {
        log.info("Creating new [{}] gauge for station: [{}]", metricName, station.getName());
        var tags = createTags(station, metricName);
        return registry.gauge(SENSOR_GAUGE_NAME, tags, new AtomicDouble(0));
    }

    /**
     * Increments the counter to keep track how many measurements for a certain metric have been processed.
     *
     * @param measurement the measurement that was processed and is to be counted.
     */
    public void incrementCounter(Measurement measurement) {
        var metric = measurement.getMetric();
        var station = measurement.getStation();
        var counter = getCounter(station, metric.getName());
        counter.increment();
        log.debug("Incremented [{}] counter for station [{}]", metric.getName(), station.getName());
    }

    /**
     * Retrieve the counter for the given station name and metric. If no counter exists, a new one is created.
     *
     * @return the counter that was stored in the cache, or a new one if there was none yet.
     */
    private Counter getCounter(Station station, String metricName) {
        var key = String.format("%s.%s", station.getName(), metricName);
        return counters.computeIfAbsent(key, s -> createCounter(station, metricName));
    }

    /**
     * Create a counter for the given station and metric.
     *
     * @return the counter that was created.
     */
    private Counter createCounter(Station station, String metricName) {
        log.info("Creating new measurement reports counter for station: [{}]", station.getName());
        var tags = createTags(station, metricName);
        return registry.counter(MEASUREMENT_COUNTER, tags);
    }

    private List<Tag> createTags(Station station, String metricName) {
        return List.of(
            Tag.of("station", station.getName()),
            Tag.of("indoor", String.valueOf(station.isIndoor())),
            Tag.of("metric", metricName)
        );
    }

    /**
     * Check for idle gauges at a scheduled interval. Removes gauges when the time since it's last update was past the allowed idle time.
     */
    @Scheduled(fixedRateString = "${rcm.monitoring.idle-gauge-cleanup-interval-ms}")
    public void removeIdleGauges() {
        log.trace("Looking for idle gauges");

        var idleGauges = getIdleGauges();

        idleGauges.forEach(key -> {
            var gaugesToRemove = findGaugesByKey(key);
            gaugesToRemove.forEach(gauge -> {
                registry.remove(gauge);
                log.info("Removed gauge from registry: [{}]", gauge.getId());
            });
            gauges.remove(key);
            gaugesUpdatedAt.remove(key);
        });
    }

    private Collection<Gauge> findGaugesByKey(String key) {
        var station = key.split("\\.")[0];
        var metric = key.split("\\.")[1];
        return registry
            .find(SENSOR_GAUGE_NAME)
            .tag("station", station)
            .tag("metric", metric)
            .gauges();
    }

    /**
     * Returns the keys of every gauge that has not been updated for more than max allowed idle time.
     */
    private List<String> getIdleGauges() {
        return gaugesUpdatedAt.entrySet().stream()
            .filter(entry -> System.currentTimeMillis() - entry.getValue() >= maxIdleTime)
            .map(Map.Entry::getKey)
            .toList();
    }
}
