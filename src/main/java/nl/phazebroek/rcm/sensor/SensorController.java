/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import static nl.phazebroek.rcm.security.Roles.ADMIN;

import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import nl.phazebroek.rcm.api.SensorsApi;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.api.model.GetSensorsResponse;
import nl.phazebroek.rcm.api.model.UpdateSensorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
class SensorController implements SensorsApi {

    private final SensorService sensorService;

    @Override
    public ResponseEntity<GetSensorsResponse> getSensors() {
        var sensors = sensorService.getSensors();
        return ResponseEntity.ok(new GetSensorsResponse().sensors(sensors));
    }

    @Override
    @Secured(ADMIN)
    public ResponseEntity<Void> postSensor(@Valid CreateSensorDto createSensorDto) {
        sensorService.addSensor(createSensorDto);
        return ResponseEntity.noContent().build();
    }

    @Override
    @Secured(ADMIN)
    public ResponseEntity<Void> updateSensor(UUID id, @Valid UpdateSensorDto updateSensorDto) {
        sensorService.updateSensor(id, updateSensorDto);
        return ResponseEntity.noContent().build();
    }
}
