/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.NotFoundException;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.api.model.SensorDto;
import nl.phazebroek.rcm.api.model.UpdateSensorDto;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class SensorService {

    private final SensorRepository sensorRepository;

    private final SensorMapper sensorMapper;

    private final CreateSensorDtoValidator createSensorDtoValidator;

    private final UpdateSensorDtoValidator updateSensorDtoValidator;

    public Sensor getSensor(UUID id) {
        return sensorRepository.findById(id)
            .orElseThrow(() -> new NotFoundException(String.format("Sensor with id %s not found", id)));
    }

    /**
     * Return the sensors with the given names.
     */
    public List<Sensor> findSensorsByName(List<String> sensorNames) {
        return sensorRepository.findAllByNameIn(sensorNames);
    }

    /**
     * Checks if the sensor with the given name exists.
     *
     * @param sensorName the name to check.
     * @return true when a sensor with the given name was found, false otherwise.
     */
    public boolean sensorExistsWithName(String sensorName) {
        return sensorRepository.existsByName(sensorName);
    }

    /**
     * Returns a list of sensors.
     */
    List<SensorDto> getSensors() {
        var sensors = sensorRepository.findAll();
        return sensorMapper.toSensorDtos(sensors);
    }

    /**
     * Persist sensor.
     *
     * @throws BadRequestException when request is not valid.
     */
    public void addSensor(CreateSensorDto createSensorDto) {
        createSensorDtoValidator.validate(createSensorDto);
        var sensor = sensorMapper.toSensor(createSensorDto);
        sensorRepository.save(sensor);
        log.info("Sensor [{}] saved", sensor.getName());
    }

    /**
     * Update the sensor.
     *
     * @param id              the id of the sensor to update
     * @param updateSensorDto the request with the new values to apply to the sensor with the given id.
     * @throws BadRequestException when request is not valid.
     * @throws NotFoundException   when the sensor does not exist.
     */
    public void updateSensor(UUID id, UpdateSensorDto updateSensorDto) {
        log.debug("Updating sensor [{}]", id);
        var sensor = getSensor(id);
        updateSensorDtoValidator.validate(updateSensorDto, id);
        if (StringUtils.hasText(updateSensorDto.getName())) {
            sensor.setName(updateSensorDto.getName());
        }
        if (!CollectionUtils.isEmpty(updateSensorDto.getMetrics())) {
            sensor.setMetrics(sensorMapper.toMetrics(updateSensorDto.getMetrics()));
        }
        sensorRepository.save(sensor);
    }
}
