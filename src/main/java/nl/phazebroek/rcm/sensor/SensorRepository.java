/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorRepository extends JpaRepository<Sensor, UUID> {

    /**
     * Check if a sensor with the given name exists.
     */
    boolean existsByName(String name);

    /**
     * Look for the sensor with the given name.
     */
    Optional<Sensor> findByName(String name);

    /**
     * Find all the sensors with all of the given names.
     */
    List<Sensor> findAllByNameIn(List<String> sensorNames);
}
