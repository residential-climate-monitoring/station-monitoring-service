/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import nl.phazebroek.rcm.metric.Metric;

/**
 * A sensor that is capable of sensing at least one metric (a physical quantity). Sensors are added to stations for monitoring.
 */
@Entity
@Getter
@Setter
@ToString
public class Sensor {

    @Id
    @GeneratedValue(generator = "system-uuid")
    private UUID id;

    @Column(length = 50, unique = true)
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Metric> metrics;

}
