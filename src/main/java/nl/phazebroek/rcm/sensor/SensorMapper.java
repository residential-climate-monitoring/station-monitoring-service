/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.api.model.SensorDto;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.metric.MetricRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
abstract class SensorMapper {

    @Autowired
    private MetricRepository metricRepository;

    @Mapping(target = "id", ignore = true)
    abstract Sensor toSensor(CreateSensorDto source);

    abstract SensorDto toSensorDto(Sensor source);

    abstract List<SensorDto> toSensorDtos(List<Sensor> source);

    /**
     * Convert a list of metric names to the actual metrics. Assuming the sensor is already validated to contain valid metric names.
     * Duplicate metric names will result in only one metric instance.
     */
    protected Set<Metric> toMetrics(List<String> metricNames) {
        if (metricNames == null) {
            return new HashSet<>();
        }

        return metricRepository.findAllByNameIn(metricNames);
    }

    /**
     * Convert a list of metrics to the human friendly readable name.
     */
    protected List<String> toMetricNames(Set<Metric> metrics) {
        if (CollectionUtils.isEmpty(metrics)) {
            return new ArrayList<>();
        }
        return metrics.stream()
            .map(Metric::getName)
            .sorted()
            .toList();
    }

}
