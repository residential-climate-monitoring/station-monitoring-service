/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.CreateSensorDto;
import nl.phazebroek.rcm.metric.MetricValidator;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class CreateSensorDtoValidator {

    protected final SensorRepository sensorRepository;

    protected final MetricValidator metricValidator;

    /**
     * Validates if the request is valid.
     *
     * @throws BadRequestException when not valid.
     */
    public void validate(CreateSensorDto createSensorDto) {
        log.debug("Validating CreateSensorRequest");
        metricValidator.validate(createSensorDto.getMetrics());
        validateUniqueName(createSensorDto.getName());
    }

    private void validateUniqueName(String name) {
        log.debug("Validating uniqueness of name: [{}]", name);
        if (sensorRepository.existsByName(name)) {
            throw new BadRequestException("Name of the sensor must be unique");
        }
    }

}
