/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.sensor;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.UpdateSensorDto;
import nl.phazebroek.rcm.metric.MetricValidator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateSensorDtoValidator {

    protected final SensorRepository sensorRepository;

    protected final MetricValidator metricValidator;

    /**
     * Validates if the request is valid.
     *
     * @param updateSensorDto the request to validate.
     * @param id              the id of the sensor that is being updated. If the name is not unique and it belongs to a sensor with
     *                        another id, it's already taken and not available.
     */
    public void validate(UpdateSensorDto updateSensorDto, UUID id) {
        log.debug("Validating UpdateSensorRequest");
        var isNonEmptyValue = false;
        if (!CollectionUtils.isEmpty(updateSensorDto.getMetrics())) {
            metricValidator.validate(updateSensorDto.getMetrics());
            isNonEmptyValue = true;
        }
        if (StringUtils.hasText(updateSensorDto.getName())) {
            validateUniqueName(updateSensorDto.getName(), id);
            isNonEmptyValue = true;
        }
        if (!isNonEmptyValue) {
            throw new BadRequestException("At least one value required.");
        }
    }

    private void validateUniqueName(String name, UUID id) {
        log.debug("Validating uniqueness of name: [{}]", name);
        sensorRepository.findByName(name)
            .filter(sensor -> !sensor.getId().equals(id))
            .ifPresent(sensor -> {
                throw new BadRequestException("Name of the sensor must be unique");
            });
    }
}
