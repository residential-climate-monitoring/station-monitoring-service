/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import nl.phazebroek.rcm.monitoring.MonitoringService;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MeasurementService {

    private final MeasurementReportValidator measurementReportValidator;

    private final MeasurementMapper measurementMapper;

    private final MonitoringService monitoringService;

    /**
     * Validate and process the measurement report for monitoring. The station from which the report was received must be known to the
     * service and the measurements must be supported by the sensors connected to the station.
     */
    public void processMeasurementReport(MeasurementReportDto measurementReportDto) {
        measurementReportValidator.validate(measurementReportDto);
        var measurements = measurementMapper.toMeasurements(measurementReportDto);
        measurements.forEach(monitoringService::updateGauge);
        measurements.forEach(monitoringService::incrementCounter);
    }

}
