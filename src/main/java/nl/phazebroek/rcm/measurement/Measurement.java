/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import lombok.Value;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.station.Station;

@Value
public class Measurement {

    Station station;

    Metric metric;

    double value;

}
