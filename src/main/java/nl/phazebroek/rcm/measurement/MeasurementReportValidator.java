/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.station.Station;
import nl.phazebroek.rcm.station.StationService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Slf4j
@Component
@RequiredArgsConstructor
class MeasurementReportValidator {

    private final StationService stationService;

    /**
     * Validates a measurement report by checking if the metrics included in the report are supported by the station it was submitted for.
     *
     * @throws BadRequestException in case one or multiple metrics are not supported by the station.
     */
    public void validate(MeasurementReportDto measurementReportDto) {
        var stationName = measurementReportDto.getStationName();
        log.debug("Validating measurement report from station [{}]", stationName);
        var station = getStation(stationName);
        validateStationSupportsMetrics(measurementReportDto, station);
    }

    private Station getStation(String stationName) {
        return stationService
            .findStationByName(stationName)
            .orElseThrow(() -> new BadRequestException(String.format("Unknown station '%s'", stationName)));
    }

    private void validateStationSupportsMetrics(MeasurementReportDto measurementReportDto, Station station) {
        var supportedMetrics = getSupportedMetrics(station);
        var includedMetrics = measurementReportDto.getMeasurements().keySet();
        var invalidMetrics = getInvalidMetrics(supportedMetrics, includedMetrics);
        if (!CollectionUtils.isEmpty(invalidMetrics)) {
            var joinedInvalidMetrics = includedMetrics.stream().sorted().collect(joining(", "));
            throw new BadRequestException(
                String.format("Station '%s' does not support the following metric(s): '%s'", station.getName(), joinedInvalidMetrics));
        }
    }

    private Set<String> getInvalidMetrics(Set<String> supportedMetrics, Set<String> includedMetrics) {
        return includedMetrics.stream()
            .filter(includedMetric -> !supportedMetrics.contains(includedMetric))
            .collect(toSet());
    }

    private Set<String> getSupportedMetrics(Station station) {
        return stationService
            .getSupportedMetrics(station)
            .stream()
            .map(Metric::getName)
            .collect(toSet());
    }
}
