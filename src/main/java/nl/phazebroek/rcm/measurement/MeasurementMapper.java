/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import java.util.ArrayList;
import java.util.List;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.metric.MetricRepository;
import nl.phazebroek.rcm.station.StationService;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
abstract class MeasurementMapper {

    @Autowired
    private StationService stationService;

    @Autowired
    private MetricRepository metricRepository;

    /**
     * Converts the report to a list of measurements. Assumes the report was validated and the station is known! returns null if the
     * measurement report is null.
     */
    List<Measurement> toMeasurements(MeasurementReportDto measurementReportDto) {
        if (measurementReportDto == null) {
            return new ArrayList<>();
        }

        var station = stationService.findStationByName(measurementReportDto.getStationName()).orElseThrow();
        return measurementReportDto.getMeasurements().entrySet().stream()
            .map(entry -> new Measurement(
                station,
                toMetric(entry.getKey()),
                entry.getValue()))
            .toList();
    }

    private Metric toMetric(String metricName) {
        return metricRepository
            .findByName(metricName)
            .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported metric: %s", metricName)));
    }
}
