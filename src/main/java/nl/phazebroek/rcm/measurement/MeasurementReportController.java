/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.measurement;

import static nl.phazebroek.rcm.security.Authorities.SUBMIT_REPORT;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import nl.phazebroek.rcm.api.MeasurementReportsApi;
import nl.phazebroek.rcm.api.model.MeasurementReportDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
class MeasurementReportController implements MeasurementReportsApi {

    private final MeasurementService measurementService;

    @Override
    @PreAuthorize("hasAnyAuthority('" + SUBMIT_REPORT + "')")
    public ResponseEntity<Void> postMeasurementReport(@Valid MeasurementReportDto measurementReportDto) {
        measurementService.processMeasurementReport(measurementReportDto);
        return ResponseEntity.noContent().build();
    }

}
