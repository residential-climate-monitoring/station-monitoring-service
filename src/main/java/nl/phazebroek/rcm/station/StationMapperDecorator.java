/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import java.util.List;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.StationDto;
import nl.phazebroek.rcm.sensor.Sensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * A decorator for the station mapper that takes the sensor settings into account.
 * Sensor settings are stored as separately and are retrieved on a per-station basis.
 */
public abstract class StationMapperDecorator extends StationMapper {

    @Autowired
    @Qualifier("delegate")
    private StationMapper delegate;

    @Override
    Station toStation(CreateStationRequest createStationRequest, List<Sensor> sensors) {
        return delegate.toStation(createStationRequest, sensors);
    }

    /**
     * Decorates a station with the list of sensors including the settings of each sensor (if any).
     */
    @Override
    StationDto toStationDto(Station station) {
        return delegate
            .toStationDto(station)
            .sensors(delegate.toSensorWithSettingsDtos(station));
    }

}
