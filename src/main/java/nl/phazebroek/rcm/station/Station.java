/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;
import nl.phazebroek.rcm.sensor.Sensor;

/**
 * A station is what is monitored. A station contains one or multiple sensors which are periodically gauged for their current value.
 */
@Entity
@Getter
@Setter
public class Station {

    @Id
    @GeneratedValue(generator = "system-uuid")
    private UUID id;

    @Column(length = 50, unique = true)
    private String name;

    private boolean indoor;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Sensor> sensors;
}
