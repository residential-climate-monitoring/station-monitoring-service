/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.BadRequestException;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;
import nl.phazebroek.rcm.sensor.SensorService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Slf4j
@Component
@RequiredArgsConstructor
class StationValidator {

    private final SensorService sensorService;

    private final StationRepository stationRepository;

    /**
     * Validates if the sensors of the station all exists.
     *
     * @throws BadRequestException in case invalid sensors are found.
     * @implNote for each sensor name a query is made to the database to check if it exists. This is a bad
     *     practice from performance point of view, but the maximum number of sensors is limited and knowning which
     *     sensor names are invalid without the need to fully serialize the sensor entities is more valuable now.
     */
    public void validate(CreateStationRequest createStationRequest) {
        log.info("Validating station [{}]", createStationRequest.getName());

        validateUniqueName(createStationRequest.getName());
        validateSensorsExist(createStationRequest.getSensors());
    }

    /**
     * Validate the request to update a station. It must contain at least one property for the station to update.
     * If the sensors are updated, it checks that all sensors exist.
     */
    public void validate(UpdateStationRequest updateStationRequest) {
        validateAtLeastOnePropertyIsUpdated(updateStationRequest);

        if (updateStationRequest.getSensors() != null) {
            validateSensorsExist(updateStationRequest.getSensors());
        }
    }

    private void validateAtLeastOnePropertyIsUpdated(UpdateStationRequest updateStationRequest) {
        if (CollectionUtils.isEmpty(updateStationRequest.getSensors()) && updateStationRequest.getIndoor() == null) {
            throw new BadRequestException("To update a station, provide at least one property with a new value.");
        }
    }

    /**
     * Check there are _no_ stations with the given name yet.
     *
     * @param name the name of the station to check for uniqueness.
     * @throws BadRequestException if the name is not unique.
     */
    private void validateUniqueName(String name) {
        if (stationRepository.existsByName(name)) {
            throw new BadRequestException("Name of the station must be unique");
        }
    }

    /**
     * Verify the sensors exists.
     *
     * @param sensorsWithSettings the list of sensors to verify.
     * @throws BadRequestException if one or more sensors do not exist.
     */
    private void validateSensorsExist(List<SensorWithSettingsDto> sensorsWithSettings) {
        String invalidSensorNames = getInvalidSensorNames(sensorsWithSettings);

        if (StringUtils.hasText(invalidSensorNames)) {
            throw new BadRequestException(String.format("Invalid sensor(s): %s", invalidSensorNames));
        }
    }

    private String getInvalidSensorNames(List<SensorWithSettingsDto> sensorsWithSettings) {
        return sensorsWithSettings.stream()
            .map(SensorWithSettingsDto::getName)
            .filter(sensorName -> !sensorService.sensorExistsWithName(sensorName))
            .sorted()
            .collect(Collectors.joining(", "));
    }

}
