/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRepository extends JpaRepository<Station, UUID> {

    /**
     * Check if a station with the given name exists.
     */
    boolean existsByName(String name);

    /**
     * Find the station with the given name.
     *
     * @param name the name of the station to look-up.
     * @return an optional containing the station when found.
     */
    Optional<Station> findStationByName(String name);
}
