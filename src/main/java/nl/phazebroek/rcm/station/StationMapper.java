/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import java.util.ArrayList;
import java.util.List;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.api.model.StationDto;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.settings.SettingsService;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
@DecoratedWith(StationMapperDecorator.class)
abstract class StationMapper {

    @Autowired
    protected SettingsService sensorSettingsService;

    /**
     * Maps the request to create a station to the station entity.
     * ID is ignored and will be set once persisted in the database.
     */
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "sensors", source = "sensors")
    abstract Station toStation(CreateStationRequest source, List<Sensor> sensors);

    /**
     * Maps a station to a station dto, which includes a list of sensors and the settings per sensor for that particular station.
     * The {@link StationMapperDecorator} takes care of setting the sensors with settings, since the sensor settings are stored separately.
     */
    @Mapping(target = "sensors", ignore = true)
    abstract StationDto toStationDto(Station source);

    /**
     * The SensorSettings are not stored directly on a station, because they also have a relation to each sensor.
     * Map a station to a list of {@link SensorWithSettingsDto}.
     */
    List<SensorWithSettingsDto> toSensorWithSettingsDtos(Station station) {
        var sensors = station.getSensors();
        var target = new ArrayList<SensorWithSettingsDto>(sensors.size());
        sensors.forEach(sensor -> target.add(new SensorWithSettingsDto()
            .name(sensor.getName())
            .settings(sensorSettingsService.getSensorSettingsDto(station, sensor).getSettings())
        ));
        return target;
    }

}
