/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.NotFoundException;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.SensorSettingsDto;
import nl.phazebroek.rcm.api.model.SensorWithSettingsDto;
import nl.phazebroek.rcm.api.model.StationDto;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;
import nl.phazebroek.rcm.metric.Metric;
import nl.phazebroek.rcm.sensor.Sensor;
import nl.phazebroek.rcm.sensor.SensorService;
import nl.phazebroek.rcm.settings.SettingsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class StationService {

    private final StationValidator stationValidator;

    private final StationRepository stationRepository;

    private final StationMapper stationMapper;

    private final SettingsService settingsService;

    private final SensorService sensorService;

    /**
     * Find a station by name.
     *
     * @return optional with the station when found or empty otherwise.
     */
    public Optional<Station> findStationByName(String name) {
        return stationRepository.findStationByName(name);
    }

    public Station getStationByName(String name) {
        return findStationByName(name)
            .orElseThrow(() -> new NotFoundException("Station " + name + " not found"));
    }

    /**
     * Retrieve a list of metrics supported by all the sensors of the station combined.
     */
    public Set<Metric> getSupportedMetrics(Station station) {
        return station.getSensors().stream()
            .map(Sensor::getMetrics)
            .flatMap(Collection::stream)
            .collect(toSet());
    }

    /**
     * Persist station if valid.
     *
     * @return the station, including its ID.
     */
    @Transactional
    public StationDto addStation(CreateStationRequest createStationRequest) {
        log.info("Creating new station [{}]", createStationRequest.getName());
        stationValidator.validate(createStationRequest);

        var sensorNames = getSensorNames(createStationRequest.getSensors());
        var sensors = sensorService.findSensorsByName(sensorNames);
        var station = stationMapper.toStation(createStationRequest, sensors);
        station = stationRepository.save(station);

        var sensorsByName = getSensorsByName(sensors);
        var sensorWithSettingsDtos = createStationRequest.getSensors();
        createSensorSettings(station, sensorsByName, sensorWithSettingsDtos);

        log.info("Station [{}] created successfully", station.getName());
        return stationMapper.toStationDto(station);
    }

    private void createSensorSettings(Station station, Map<String, Sensor> sensorsByName,
        List<SensorWithSettingsDto> sensorWithSettingsDtos) {
        sensorWithSettingsDtos
            .forEach(sensorWithSettingsDto -> settingsService
                .createSettings(
                    station,
                    sensorsByName.get(sensorWithSettingsDto.getName()),
                    sensorWithSettingsDto.getSettings()));
    }

    private Map<String, Sensor> getSensorsByName(List<Sensor> sensors) {
        return sensors.stream().collect(toMap(Sensor::getName, sensor -> sensor));
    }

    private List<String> getSensorNames(List<SensorWithSettingsDto> sensors) {
        return sensors.stream()
            .map(SensorWithSettingsDto::getName)
            .toList();
    }

    public List<Station> getStations() {
        return stationRepository.findAll();
    }

    List<StationDto> getStationDtos() {
        var stations = stationRepository.findAll();
        log.info("Found [{}] stations", stations.size());
        return stations.stream()
            .map(stationMapper::toStationDto)
            .toList();
    }

    /**
     * Update a single station. The name cannot be changed and acts as the identifier.
     */
    @Transactional
    public void updateStation(String stationName, UpdateStationRequest updateStationRequest) {
        log.info("Updating station [{}]", stationName);
        var station = getStationByName(stationName);
        stationValidator.validate(updateStationRequest);
        if (updateStationRequest.getSensors() != null) {
            updateStationSensors(updateStationRequest, station);
        }
        if (updateStationRequest.getIndoor() != null) {
            updateStationIndoor(updateStationRequest, station);
        }
        stationRepository.save(station);
        log.info("Station [{}] updated successfully", station.getName());
    }

    private void updateStationSensors(UpdateStationRequest updateStationRequest, Station station) {
        var sensorNames = getSensorNames(updateStationRequest.getSensors());
        var sensors = sensorService.findSensorsByName(sensorNames);
        station.setSensors(sensors);
        updateStationSensorSettings(updateStationRequest, station, sensors);
        log.info("Updating sensors of station [{}] to [{}]", station.getName(), sensors);
    }

    private void updateStationSensorSettings(UpdateStationRequest updateStationRequest, Station station, List<Sensor> sensors) {
        var sensorsByName = getSensorsByName(sensors);
        var sensorWithSettingsDtos = updateStationRequest.getSensors();
        sensorWithSettingsDtos.forEach(sensorWithSettingsDto ->
            saveSensorSettings(station, sensorsByName.get(sensorWithSettingsDto.getName()), sensorWithSettingsDto.getSettings()));
    }

    private void updateStationIndoor(UpdateStationRequest updateStationRequest, Station station) {
        station.setIndoor(updateStationRequest.getIndoor());
        log.info("Updating indoor flag of station [{}] to [{}]", station.getName(), updateStationRequest.getIndoor());
    }

    SensorSettingsDto getSensorSettingsDto(String stationName, String sensorName) {
        var station = getStationByName(stationName);
        var sensor = getSensorByNameFromStation(station, sensorName);
        return settingsService.getSensorSettingsDto(station, sensor);
    }

    void saveSensorSettings(String stationName, String sensorName, SensorSettingsDto sensorSettingsDto) {
        var station = getStationByName(stationName);
        var sensor = getSensorByNameFromStation(station, sensorName);
        saveSensorSettings(station, sensor, sensorSettingsDto.getSettings());
    }

    private void saveSensorSettings(Station station, Sensor sensor, Map<String, Object> settings) {
        settingsService.updateSettings(station, sensor, settings);
    }

    private Sensor getSensorByNameFromStation(Station station, String sensorName) {
        return station.getSensors().stream()
            .filter(s -> s.getName().equals(sensorName))
            .findAny()
            .orElseThrow(() -> new NotFoundException("Station " + station.getName() + " does not have sensor " + sensorName));
    }
}
