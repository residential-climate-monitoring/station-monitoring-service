/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm.station;

import static nl.phazebroek.rcm.security.Roles.ADMIN;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.phazebroek.rcm.api.StationsApi;
import nl.phazebroek.rcm.api.model.CreateStationRequest;
import nl.phazebroek.rcm.api.model.GetStationsResponse;
import nl.phazebroek.rcm.api.model.SensorSettingsDto;
import nl.phazebroek.rcm.api.model.StationDto;
import nl.phazebroek.rcm.api.model.UpdateStationRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
class StationController implements StationsApi {

    private final StationService stationService;

    private final StationMapper stationMapper;

    @Override
    @Secured(ADMIN)
    public ResponseEntity<StationDto> postStation(@Valid CreateStationRequest createStationRequest) {
        var savedStation = stationService.addStation(createStationRequest);
        return ResponseEntity.ok(savedStation);
    }

    @Override
    public ResponseEntity<GetStationsResponse> getStations() {
        log.info("Get stations");
        var stations = stationService.getStationDtos();
        return ResponseEntity.ok(new GetStationsResponse().stations(stations));
    }

    @Override
    public ResponseEntity<StationDto> getStation(@Pattern(regexp = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-\\s]*$") @Size(max = 50) String stationName) {
        log.info("Get station [{}]", stationName);
        var station = stationService.getStationByName(stationName);
        return ResponseEntity.ok(stationMapper.toStationDto(station));
    }

    @Override
    @Secured(ADMIN)
    public ResponseEntity<Void> updateStation(@Pattern(regexp = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-\\s]*$") @Size(max = 50) String station,
        @Valid UpdateStationRequest updateStationRequest) {
        stationService.updateStation(station, updateStationRequest);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<SensorSettingsDto> getSensorSettings(
        @Pattern(regexp = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-\\s]*$") @Size(max = 50) String station,
        @Pattern(regexp = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-\\s]*$") @Size(max = 50) String sensor) {
        var sensorSettingsDto = stationService.getSensorSettingsDto(station, sensor);
        return ResponseEntity.ok(sensorSettingsDto);
    }

    @Override
    @Secured(ADMIN)
    public ResponseEntity<Void> setSensorSettings(@Pattern(regexp = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-\\s]*$") @Size(max = 50) String station,
        @Pattern(regexp = "^[a-zA-Z0-9]{1}[a-zA-Z0-9-\\s]*$") @Size(max = 50) String sensor, @Valid SensorSettingsDto sensorSettingsDto) {
        stationService.saveSensorSettings(station, sensor, sensorSettingsDto);
        return ResponseEntity.noContent().build();
    }
}
