/*
 * Copyright (c) 2020 Pim Hazebroek
 * This program is made available under the terms of the MIT License.
 * For full details check the LICENSE file at the root of the project.
 */

package nl.phazebroek.rcm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@SpringBootApplication
@EnableScheduling
public class StationMonitoringServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StationMonitoringServiceApplication.class);
        log.info("Residential Climate Monitoring Station Monitoring Service started successfully");
    }

}
