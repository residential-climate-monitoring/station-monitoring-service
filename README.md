![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/residential-climate-monitoring/station-monitoring-service/master?logo=gitlab)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=alert_status)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=security_rating)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=ncloc)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=coverage)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=bugs)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=sqale_index)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=residential-climate-monitoring_station-monitoring-service&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=residential-climate-monitoring_station-monitoring-service)

# Station Monitoring Service

Service to manage the stations and sensors and collect measurements from those stations. Measurements are monitored by Prometheus and can be
visualized using Grafana.

This service is based on Java 14 and made available as a docker image compatible with 32-bit ARM architecture, so it can be run on the
Raspberry Pi or other similar hardware.

For more details about installing, running and configuring the service check the wiki.

# Automatic cleanup of gauges

The gauges that track the current value of station sensors are just a snapshot of the current value when a measurement report is submitted.
These values are only valid for the moment at which they are taken. Therefore, when a gauge is not updated for some time, the gauge is
removed automatically. How often and after how long idle time gauges are removed can be configured using:

* `rcm.monitoring.idle-gauge-cleanup-interval-ms` - the interval time in milliseconds at which the cleanup is scheduled. Defaults to 1
  minute.
* `rcm.monitoring.max-idle-time-ms` - the time in milliseconds for gauges between measurement reports to be submitted. When this period is
  exceeded at the next cleanup, the gauge is removed. Defaults to 5 minutes.

# Sensor settings

The Station Client can be configured with a local .env file. To make it easier to apply settings to sensors at a certain station, they can
also be stored in the Station Monitoring Service. Sensor settings can be applied when creating or updating stations. For example to create a
station including sensor settings, make a `POST` request to `/stations` with the following payload:

```json
{
  "name": "test",
  "indoor": true,
  "sensors": [
    {
      "name": "SHT31-D"
    },
    {
      "name": "TSL2591",
      "settings": {
        "tsl2591-gain": "GAIN_LOW",
        "tsl2591-integration-time": "100ms"
      }
    }
  ]
}
```

It is also possible to set/update settings directly for a specific sensor at a specific station. To apply the same settings as in the
example above, make a `PUT` request to: `/stations/test/sensors/TSL2591/settings` with the following payload:

```json
{
  "settings": {
    "tsl2591-gain": "GAIN_LOW",
    "tsl2591-integration-time": "100ms"
  }
}
```

Note: this will override any previous settings. It is not possible to update just a single setting, all settings must be provided in each
request if you want to preserve them. A `GET` request to the same endpoint will just return the current settings.

# Sensor settings

The Station Client can be configured with a local .env file. To make it easier to apply settings to sensors at a certain station, they can
also be stored in the Station Monitoring Service. Sensor settings can be applied when creating or updating stations. For example to create a
station including sensor settings, make a `POST` request to `/stations` with the following payload:

```json
{
  "name": "test",
  "indoor": true,
  "sensors": [
    {
      "name": "SHT31-D"
    },
    {
      "name": "TSL2591",
      "settings": {
        "tsl2591-gain": "GAIN_LOW",
        "tsl2591-integration-time": "100ms"
      }
    }
  ]
}
```

It is also possible to set/update settings directly for a specific sensor at a specific station. To apply the same settings as in the
example above, make a `PUT` request to: `/stations/test/sensors/TSL2591/settings` with the following payload:

```json
{
  "settings": {
    "tsl2591-gain": "GAIN_LOW",
    "tsl2591-integration-time": "100ms"
  }
}
```

Note: this will override any previous settings. It is not possible to update just a single setting, all settings must be provided in each
request if you want to preserve them. A `GET` request to the same endpoint will just return the current settings.

# Default metrics

The service comes with a set of default metrics and units that are ready to be used.
These metrics are created on start-up when not present yet. If desired, this can be disabled by setting `rcm.metrics.auto-create-enabled` to false.

# Philips Hue Integration

It is possible to monitor sensors via the Philips Hue Bridge API, connected on the same network.
To enable Philips Hue integration, create an account on your Philips Hue bridge via [developers.meethue.com](https://developers.meethue.com/) 
and configure the base-url of the API in the service like this: 
```yaml
rcm.hue.bridge.base-url: http://192.168.0.123/api/username
```
Note: replace the IP address and username with that of your bridge.

The following sensors are supported:
- [Motion sensor](https://www.philips-hue.com/en-gb/p/hue-motion-sensor/8718696743171) (model `SML001`)
- [Outdoor sensor](https://www.philips-hue.com/en-gb/p/hue-outdoor-sensor/8718699625474) (model `SML002`)

For every Philips Hue sensor, a station is created automatically with the same name as the Hue Sensor.
Sensors and metrics are also created and assigned automatically upon creation of the station.
The name of the Philips Hue Sensor in RCM can be tweaked via the `rcm.hue.motion-sensor.default-name` setting.
Auto creation of sensors can be disabled by setting `rcm.sensors.auto-create-enabled` to false.

The status of the sensors are retrieved from the bridge every minute. The motion sensors presence state is (near) real-time. This means that 
the sensor may report that no presence is detected at the time the state is queried, even though there was motion between the moment the request
was made and the previous request one minute ago. To avoid missing motion detection in the reporting, it will not only look at the state but
also at the time it was last updated.

## Customizing the name of the Hue Motion Sensor in RCM

To change the name of the sensor that represents the Philips Hue Motion Sensor within RCM, override: `rcm.hue.motion-sensor.default-name`.

# Upgrading

To upgrade the Station Monitoring Service to a newer version:

* Change version in the docker-compose.yml file.
* Run: `docker-compose stop rcm && docker-compose pull rcm && docker-compose up -d rcm`
  This will stop the `rcm` container, pull the version specified and start it again in the background.

Note: for major releases

# License

The Residential Monitoring Service is distributed under the MIT License
