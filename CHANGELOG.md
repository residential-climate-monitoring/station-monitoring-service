# Changelog

All notable changes to this project will be documented in this file. The format is based
on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

* Added settings for sensors per station. This is just a simple key-value map which can be fetched by the station client to apply when
  performing measurements.
* Added `GET /stations/{name}` endpoint to fetch details of a single station.
* Added `GET /stations/{stationName}/sensors/{sensorName}/settings` endpoint to fetch settings of specific sensor at specific station.
* Added `PUT /stations/{stationName}/sensors/{sensorName}/settings` endpoint to set/update settings of specific sensor at specific station.
  This overrides any previous settings for that sensor on that station.
* Added `PUT /sensors/{id}` endpoint to make changes to an existing sensor.
  * Automatically create default metrics and sensors when non-existent.
* Integration with Philips Hue Bridge API to monitor Philips Hue Sensors.
* Added `/metrics` endpoint with GET and POST operations to list and create metrics. 
  The metrics in the GET response are sorted by name in ascending order.
* Added `/metrics/{id}` endpoint with PATCH operation to update a metric.
* Added the following default metrics to support the SGP30 sensor:
  * `eCO2` - in ppm
  * `tVOC` - in ppb
  * `raw-H2` - no unit
  * `raw-ethanol` - no unit
* Added the following default metrics to support the SYS sensor:
  * `disk-total` - in bytes
  * `disk-free` - in bytes
  * `disk-used` - in bytes
  * `mem-total` - in bytes
  * `mem-available` - in bytes
  * `mem-used` - in bytes
  * `load-avg-1` - no unit
  * `load-avg-5` - no unit
  * `load-avg-15` - no unit
  * `net-bytes-sent` - in bytes
  * `net-bytes-recv` - in bytes
  * `net-packets-sent` - no unit
  * `net-packets-recv` - no unit
  * `net-errin` - no unit
  * `net-errout` - no unit
  * `net-dropin` - no unit
  * `net-dropout` - no unit
  * `cpu-count` - no unit
  * `cpu-frequency` - in MHz
  * `cpu-percent` - in %
  * `cpu-temp` - in degree Celsius
  * `sys-boot-time` - as epoch timestamp

### Changed

* Upgraded to Java 17.
* Upgraded to Grafana 7.4.5
* Upgraded to Prometheus 2.26.0
* Upgraded to Postgres 12.5
* Upgraded to Spring Boot 2.4.5
* Set grafana setting `login_maximum_lifetime_duration` to 365 days
* BREAKING:
  * The `sensors` field for the create and update station endpoint is changed from an array of strings to an array of objects. Each object
    must at least contain the field `name` with name of the sensor and optionally can contain the field `settings` which is a map with
    key-value pairs.

### Fixed

* Added environment variables for SMTP settings for Grafana in the docker-compose file so Grafana is able to send e-mail alerts.
* Gauges are now automatically removed when they did not receive an update for a certain amount of time. The interval to clean up idle
  gauges, and the time they are allowed to be idle is configurable. This will avoid a flat line to appear in Grafana for gauges that no
  longer receive updates.

## [2.1.0]

### Added

* boolean flag `indoor` for stations to indicate whether they are inside or outside. This allows for more fine-grained control on monitoring
  alerts that only apply to indoor stations by including the metric tag in the query. (default: true)

To migrate from older versions: Run `src/main/resources/sql/update-2.1.0.sql`.

## [2.0.0]

### Added

* Support for the TSL2591 sensor which includes the following new metrics: `lux`, `visible-light` and `IR-light`.
* Security with Auth0, enabled by default. All GET endpoints require a valid token. All non-GET endpoints require ADMIN role, except for the
  endpoint to submit measurement-reports. This accepts an M2M access token with the `submit:report` scope.
* Very simple login page. Note: still requires a tool like postman (with cookie sync) to call endpoints.
* New endpoint to update existing stations: `PUT /stations/{stationName}`

### Changed

* Switched to https.

### Fixed

* Bug causing ConcurrentModificationException when processing measurement-reports.

## [1.0.4]

Introduced counter to monitor the number of measurements per station and metric coming in via the /measurement-reports endpoint. For each
metric in the measurement report, the corresponding counter will be incremented.

### Added

New counter with name `rcm.measurement.counter`. Containing two tags: `station` and `metric`.

## [1.0.3]

### Changed

Downgraded Java to JDK 12 due to high CPU on ARM issue, see: https://bugs.openjdk.java.net/browse/JDK-8231612.

### Added

All metrics now have the 'application' tag.

## [1.0.2]

Improved validation and updated jdk base image.

### Added

Validation on the uniqueness of the name of station and sensor is added. Now a nice error message is shown instead of failing on a unique
constraint violation from the database.

### Changed

Updated the base image to OpenJDK 13.0.2+8.

## [1.0.1]

Patch for the 1.0.0 to resolve a lazy loading issue related due to disabling OSIV (Open Session In View).

## [1.0.0]

Initial release. Supports monitoring of temperature (in Celcius), humidity, air pressure (in hecto pascal) and VOC gas
(in Ohms).

[2.1.0]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-2.1.0

[2.0.0]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-2.0.0

[1.0.4]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-1.0.4

[1.0.3]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-1.0.3

[1.0.2]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-1.0.2

[1.0.1]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-1.0.1

[1.0.0]: https://gitlab.com/residential-climate-monitoring/station-monitoring-service/-/releases#station-monitoring-service-1.0.0

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/

[Semantic Versioning]: https://semver.org/spec/v2.0.0.html